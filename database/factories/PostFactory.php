<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use App\User;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'desc'=>$faker->text,
        'contact_number'=>$faker->phoneNumber,
        'user_id'=>User::inRandomOrder()->first()->id,
    ];
});

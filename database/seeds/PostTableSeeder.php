<?php

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\User;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Post::class,30)->create();
    	
        // Post::create([
        // 	'user_id'=>User::inRandomOrder()->first()->id,
        // 	'title'=>'Post Title',
        // 	'desc'=>'Post Description',
        // 	'contact_number'=>'01091701810'
        // ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image')->default('assets/images/users/default.png');
            $table->boolean('is_social')->default(0);
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('provider')->nullable();//For Social Login
            $table->string('provider_id')->nullable();//For Social Login
            $table->string('provider_image')->nullable();//For Social Login
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

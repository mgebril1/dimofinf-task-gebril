<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

    Route::post('login', 'AuthController@login');  //User login with mobile number and password
    Route::post('register', 'AuthController@register'); //User can register

    Route::post('change-password', 'AuthController@changePassword'); //Change Password
    Route::post('forget_password', 'AuthController@forget_password'); //send password forget code by sending sms

    Route::group(['prefix'=>'posts'] , function () {
        Route::post('/','PostController@store')->middleware('jwt');
        Route::get('/','PostController@index');
        Route::get('/{post_id}','PostController@show');
    });





// });

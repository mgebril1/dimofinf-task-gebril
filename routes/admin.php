<?php
Route::group(['middleware' => 'active'], function () {
    Route::get('/', [
        'uses' => 'DashboardController@dashboard',
        'as' => 'dashboard',
    ]);
    Route::post('ckeditor/upload', 'DashboardController@upload')->name('ckeditor.upload');
    Route::get('datatable/translations', 'DashboardController@datatableTranslation');

    Route::get('/settings', 'DashboardController@getSettings')->name('getSettings');
    Route::patch('/post-settings', 'DashboardController@postSettings')->name('postSettings');

    Route::get('/affiliate-settings', 'AffiliateSettingController@index')->name('affiliate-settings');
    Route::patch('/post-saffiliate-ettings', 'AffiliateSettingController@update')->name('affiliate-settings-update');

    Route::get('password/change', 'DashboardController@getChangePasswordForm');
    Route::post('/password/change', 'DashboardController@changePassword')->name('password.change');

    Route::resource('/admins', 'AdminController');
    Route::post('data/admins', 'AdminController@getData');

    Route::resource('/roles', 'RoleController'); //d
    Route::post('data/roles', 'RoleController@getData');//d

    Route::resource('/objects', 'ObjectController'); //d
    Route::post('data/objects', 'ObjectController@getData');//d

    Route::resource('/languages', 'LanguageController');
    Route::get('data/languages', 'LanguageController@getData');
    Route::post('data/languages/{id}', 'TranslationController@getData');//
    Route::get('/languages/{language}/{word}', 'TranslationController@translation');
    Route::post('translate', 'TranslationController@translate');

    Route::resource('/pages', 'PageController');

    Route::resource('/categories', 'CategoryController'); //d
    Route::post('data/categories', 'CategoryController@getData');//d
    Route::post('upload-category-images', 'CategoryController@upload_images');//d
    Route::post('list-category-images', 'CategoryController@list_images');//d
    Route::post('category-remove-image', 'CategoryController@remove_image');//d
    //
    //
    Route::resource('/packages', 'PackageController'); //d
    Route::post('data/packages', 'PackageController@getData');//d
    //
    Route::resource('/promo_codes', 'PromoCodeController'); //d
    Route::post('data/promo_codes', 'PromoCodeController@getData');//d
    Route::get('generate/promo_codes', 'PromoCodeController@generate');//d

    Route::resource('/events', 'EventController'); //d
    Route::post('data/events', 'EventController@getData');//d
    Route::get('/events/preview/{id}/{name}', 'EventController@preview'); //d
    Route::post('data/invitations', 'InvitationController@getData');//d

    Route::resource('/orders', 'OrderController'); //d
    Route::post('data/orders', 'OrderController@getData');//d


    Route::resource('/features', 'FeatureController'); //d
    Route::post('data/features', 'FeatureController@getData');//d

    Route::resource('/countries', 'CountryController');
    Route::post('data/countries', 'CountryController@getData');

    Route::resource('/cities', 'CityController');
    Route::post('data/cities', 'CityController@getData');

    Route::resource('/users', 'UserController');
    Route::post('data/users', 'UserController@getData');

    Route::get('affilators/export', 'AffilatorController@export');
    Route::resource('/affilators', 'AffilatorController');
    Route::post('/affilators/{id}/pay', 'AffilatorController@pay');
    Route::post('data/affilators', 'AffilatorController@getData');
    Route::resource('/transactions', 'AffilatorTransactionController');
    Route::post('data/transactions', 'AffilatorTransactionController@getData');

    Route::resource('/messages', 'MessageController');
    Route::post('data/messages', 'MessageController@getData');

    Route::resource('subscribers', 'SubscribeController');
    Route::post('data/subscribers', 'SubscribeController@getData');

    Route::resource('blogs', 'BlogController');
    Route::post('data/blogs', 'BlogController@getData');

    Route::resource('offers', 'OfferController');
    Route::post('data/offers', 'OfferController@getData');

    Route::resource('ads', 'AdController');
    Route::post('data/ads', 'AdController@getData');

    Route::resource('works', 'WorkController');
    Route::post('data/works', 'WorkController@getData');

    Route::resource('partners', 'PartnerController');
    Route::post('data/partners', 'PartnerController@getData');

    Route::resource('jobs', 'JobController');
    Route::post('data/jobs', 'JobController@getData');

    Route::resource('testimonials' , 'TestimonialController');
    Route::post('data/testimonials', 'TestimonialController@getData');


    Route::resource('galleries', 'GalleryController');
    Route::post('data/galleries', 'GalleryController@getData');
    Route::post('upload-file', 'GalleryController@upload');
    Route::post('remove-file', 'GalleryController@remove');
    Route::post('list-gallery', 'GalleryController@list');




});
Route::get('unauthorized',function () {
    return view('admin.errors.405');
});
Route::get('401',function () {
    $localization = Request::segment(1);
    $local = substr($localization, 0, 2);
    return view('admin.errors.401',compact('local'));
});
Route::fallback(function () {
    return view('admin.errors.404');
});

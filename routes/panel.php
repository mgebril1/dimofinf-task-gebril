<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index')->name('index');
Route::resource('posts','PostController');

Route::get('profile','UserController@profile');

Route::get('/edit-profile','UserController@edit_profile');
Route::post('/update-profile','UserController@update_profile')->name('update_profile');

Route::get('/edit-password','UserController@change_password_form');
Route::post('/update-password','UserController@change_password')->name('change_password');


<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
	return redirect('panel');
});
Route::group(['middleware'=>['web']], function () {
    Route::get('register/{code}', 'Auth\RegisterController@showRegistrationForm');
    Auth::routes();
    Route::get('/facebook_login', 'Auth\SocialAuthFacebookController@redirect');

    Route::get('/google_login', 'Auth\SocialAuthGoogleController@redirectToGoogle');
});
    Route::get('/google_callback', 'Auth\SocialAuthGoogleController@handleGoogleCallback');
    Route::get('/facebook_callback', 'Auth\SocialAuthFacebookController@handleFacebookCallback');

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');





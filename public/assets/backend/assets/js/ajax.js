//============== for print div  contant ================//
function PrintElem(elem)
{
    Popup($(elem).html());
}
function Popup(data)
{
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>my div</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();
    return true;
}

$(document).ready(function () {
//=================== ajax  pagnation table  ========================//
    $(document).on('click', '.ajax-pagination .pagination a', function (event) {
        event.preventDefault();
        var pageinate = $(this).attr('href');
        $.get(pageinate, function (data) {
            $('.loop').html(data);
        });
        return false;
    });//pagination

//////////////////  add by ajax //////////////
    $(document).on('submit', '.ajax-form-request', function (event) {
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');
        var thisForm = $(this);
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        // var formData   =  thisForm.serialize();
        var formData = new FormData(this);

        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#loading-button-invite').addClass('spinner-border')
                $('.btn-icon-prepend').hide()

                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled', 'disabled');
            },
            success: function (data) {
                if (data.Error.code==200) {
                    Swal.fire({
                        title: data.Error.desc,
                        text: "",
                        type: "success",
                    }).then(function (result) {
                        $('#dynamic-table-invitations').DataTable().ajax.reload();
                        
                         window.location.reload();
                    });    
                }
                if (data.Error.code==422) {
                    Swal.fire({
                        title: data.Error.desc,
                        text: "",
                        type: "error",
                    }).then(function (result) {
                         window.location.reload();
                    });    
                }
                $('#loading-button-invite').removeClass('spinner-border')
                $('.btn-icon-prepend').show()
                $(thisForm).find('#submit').removeAttr('disabled');
                

            },
            error: function (data) {
                var errors = data.responseJSON;
                errors=errors.errors;
                $.each(errors, function (index, val) {
                    $(thisForm).find('[name=' + '' + index + '' + ']').parent().addClass('tooltipx has-error ');
                    $(thisForm).find('[name=' + '' + index + '' + ']').addClass('redborder');
                    $(thisForm).find('[name=' + '' + index + '' + ']').parent().append(' <small class="text-danger  tooltiptext">' + val + '</small>');
                });

            },
            complete: function () {

            },
        });//ajax

        return false;


    });//ajax-form-request  






//update-ajax-form-request  
    $(document).on('submit', '.update-ajax-form-request', function (event) {
        event.preventDefault();
        $('.ajaxMessage').remove();
        $('*').removeClass('has-error');

        var thisForm = $(this);
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        var formData = new FormData(this);

        var hasEnctype = thisForm.attr('enctype');
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $(thisForm).find('#submit').append(' <i class="fa fa-spinner fa-pulse"></i> ').attr('disabled', 'disabled');

            },
            success: function (data) {
                if (data.status == 'true')
                {
                    $('.message').prepend('<div class=" ajaxMessage alert alert-success">' + data.message + '</div>');
                }
                else
                {
                    $('.message').prepend('<div class="alert alert-danger ajaxMessage">' + data.message + '</div>');
                }

            },
            error: function (data) {
                var errors = data.responseJSON;
                $.each(errors, function (index, val) {
                    $(thisForm).find('[name=' + '' + index + '' + ']').addClass('invalid');
                    $(thisForm).find('[name=' + '' + index + '' + ']').parent().append('<small class="error ajaxMessage">' + val + '</small>');
                });

            },
            complete: function () {
                $(thisForm).find('#submit').find('.fa').remove();
                $(thisForm).find('#submit').prop('disabled', false);
            },
        });//ajax

        return false;


    });//update-ajax-form-request   





//========= serarch input ================//
    $(document).on('keyup', '.searchInput', function (event) {
        event.preventDefault();
        var thisInput = $(this);
        var thisForm = $(this).closest('form');
        var formAction = thisForm.attr('action');
        var formMethod = thisForm.attr('method');
        var formData = thisForm.serialize();
        $.ajax({
            url: formAction,
            type: formMethod,
            dataType: 'json',
            data: formData,
            success: function (data) {
                $('.data').html(data);
            },
        });
        return false;

    });
});
$(document).on('click', '.cc', function (event) {
    var thisid = $(this).attr('id');
    if (!$(this).is(':checked')) {
        $("." + thisid).prop("checked", false);
    }
    else {
        $("." + thisid).prop("checked", true);
    }

});



function Append()
{
    var length = document.getElementById('length').value;
    var width = document.getElementById('width').value;
    var height = document.getElementById('height').value;
    var weight = document.getElementById('weight').value;
    var cover = document.getElementById('cover').value;
    var price = document.getElementById('price').value;
    var quantity = document.getElementById('quantity').value;
    if (length == "" || width == "" || height == "" || weight == "" || cover == "" || price == "" || quantity == "")
    {
        document.getElementById('errorhh').innerHTML = "Fill All Field Please";
    }
    else
    {
        document.getElementById('errorhh').innerHTML = "";
        document.getElementById('lengths').value += length + ",";
        document.getElementById('widths').value += width + ",";
        document.getElementById('heights').value += height + ",";
        document.getElementById('weights').value += weight + ",";
        document.getElementById('covers').value += cover + ",";
        document.getElementById('prices').value += price + ",";
        document.getElementById('quantitys').value += quantity + ",";
        document.getElementById('don').innerHTML += "<h5>Done</h5>";
    }


}
function Appendweight()
{
    var from = document.getElementById('from').value;
    var to = document.getElementById('to').value;
    var value = document.getElementById('value').value;
    if (from == "" || to == "" || value == "")
    {
        document.getElementById('errorhh').innerHTML = "Fill All Field Please";
    }
    else if (from <= 0 || to <= 0 || value <= 0 || from >= to) {
        document.getElementById('errorhh').innerHTML = "Invalid Values";
    }
    else
    {
        document.getElementById('errorhh').innerHTML = "";
        document.getElementById('froms').value += from + ",";
        document.getElementById('tos').value += to + ",";
        document.getElementById('values').value += value + ",";
        document.getElementById('don').innerHTML += "<h6> from : " + from + " to :" + to + " is " + value + "</h6>";
    }
}

function  Appendcolor()
{
    var color = document.getElementById('color').value;
    var qu = document.getElementById('quantity').value;
    if (color == "" || qu == "")
    {
        document.getElementById('errorhhcol').innerHTML = "Fill All Field Please";
    }
    else
    {
        document.getElementById('errorhhcol').innerHTML = "";
        document.getElementById('colors').value += color + ",";
        document.getElementById('quantitys').value += qu + ",";
        document.getElementById('doncol').innerHTML += "<h6 style='color:" + color + "'> Done </h6>";

    }
}

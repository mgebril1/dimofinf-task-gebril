<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Dimofinif Task Contain 

- 1-web app Login 
- 2-web app Register
- 3-web app Login With Facebook
- 4-web app Forget Password
- 5-web app Reset Password
- 6-web app Posts Paginated
- 7-web app Create Post
- 8-web API Login Jwt
- 8-web API Posts Paginated filtered with user_id
- 8-web API Show Single Post
- 8-web API Create New Post


## Ponus
 - using Repository Design Pattern
 - Social Login
 - Command php artisan install:project for migrate and seeding data
 - Secure your app from DDOS attack Using throttle Middleware in RouteServiceProvider

## To Run This project 
 - php version (^7.2.5|^8.0) => Required
 - Laravel version (^6.20.26) => Required
 - run composer install 
 - then run "php artisan install:project"
<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository
{
	public function __construct(Post $post)
	{
		$this->model=$post;
	}

	public function getAll($request)
	{
		$query=$this->model;
		if (isset($request->user_id)) {
			$query=$query->where('user_id',$request->user_id);
		}
		return $query->with('user')->paginate(7);
	}

	public function save($data)
	{
		$data['user_id']=auth()->user()->id;
		return $this->model->create($data);
	}

	public function find($id)
	{
		return $this->model->where('id',$id)->with('user')->first();
	}
}
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuperMailer extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $main =  $this->view($this->data['view'])
            ->from('info@eventiqr.com', 'EventQr App')
            ->subject($this->data['subject']);
        if(isset($this->data['file'])){
            $main->attachData($this->data['file'], "receipt.pdf");
        }
        if(isset($this->data['bcc'])){
            $main->cc($this->data['bcc']);
        }
        return $main;
    }
}

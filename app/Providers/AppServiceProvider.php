<?php

namespace App\Providers;

use App\Models\Country;
use Illuminate\Support\ServiceProvider;
use Schema;
use Request;
use DB;
use View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(!\App::isLocal()){
            \URL::forceScheme('https');
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

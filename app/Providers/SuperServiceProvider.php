<?php

namespace App\Providers;

use App\Repositories\SuperRepository;
use Illuminate\Support\ServiceProvider;
use App;

class SuperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('super', function (){
            return new SuperRepository();
        });
    }
}

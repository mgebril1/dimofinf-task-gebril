<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Repositories\PostRepository;
use App\Http\Requests\CreatePostApiRequest;

class PostController extends Controller
{
	public function __construct(PostRepository $postRepository)
	{
		$this->postRepository=$postRepository;
	}
    public function index(Request $request)
    {
    	$posts=$this->postRepository->getAll($request);
    	return new JsonResponse(["success" => true, "status" => 200,"data" => $posts  ,"message" => "Invalid user name or email",
            ], 200);
    }

    public function show($id)
    {
    	$post=$this->postRepository->find($id);
    	return new JsonResponse(["success" => true, "status" => 200,"data" => $post
            ], 200);
    }

    public function store(CreatePostApiRequest $request)
    {
    	$post=$this->postRepository->save($request->all());
		return new JsonResponse(["success" => true, "status" => 200,"data" => $post  ,"message" => "Post Create Successfully",
            ], 200);	
    }
}

<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        $response = new \stdClass();
        if ($validator->fails()) {

            return new JsonResponse(["success" => false, "status" => 422,"data" => $validator->errors(),"message" => 'Error in validation',
            ], 422);

        }
        if ($token  =  Auth::guard('api')->attempt(['email' => request('email'), 'password' => request('password') ]))
        {
            
            $user = Auth::guard('api')->user();
            $user->token = $token;

            return new JsonResponse(["success" => true, "status" => 200,"data" => $user  ,"message" => "Welcome :)",
            ], 200);

        } else {

            return new JsonResponse(["success" => false, "status" => 422,"data" => $response  ,"message" => "Invalid user name or email",
            ], 422);

        }
    }


    public function unauthorized(Request $request){
        return new JsonResponse(["success" => false, "status" => 401,"data" => $response  ,"message" => "Unauthorized",
            ], 401);
    }


}

<?php

namespace App\Http\Controllers\Auth
;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Language;
use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use Auth;
class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(SocialFacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
        auth()->login($user);
        return redirect()->to('/home');
    }
    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->user();

            $finduser = User::where('provider_id', $user->id)->where('provider','facebook')->first();


            if($finduser){              
                Auth::login($finduser);
                return redirect('/panel');

            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'is_social' => 1,
                    'email' => $user->email,
                    'provider'=> "facebook",
                    'provider_id'=> $user->id,
                    'provider_image'=> $user->avatar,
                    'password' => 'facebook',
                ]);
                Auth::login($newUser);
                
                return redirect('/panel');
            }

        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}
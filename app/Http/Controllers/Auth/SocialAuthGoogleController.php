<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use Illuminate\Http\Request;
use App\User;
use App\Models\Language;

class SocialAuthGoogleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle(Request $request)
    {
        @setcookie('parent_register_code',$this->register_code,0, "/");
        app()->setLocale($this->lang->code);
        if($this->country == null){
            $lang=Language::where('code',app()->getLocale())->first();
            $country=\DB::table('countries')->first();
        }else{
            $country=$this->country;
        }
        @setcookie('social_country_id',$country->id,0, "/");
        return Socialite::driver('google')->redirect();
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        
        $country_id=$_COOKIE['social_country_id'];
        // try {

            $user = Socialite::driver('google')->user();
            $finduser = User::where('provider_id', $user->id)->where('provider','google')->where('is_active',1)->first();

            if($finduser){
                if($finduser->is_active == 0)
                {
                    return redirect('/login')->with('error', \App\Help::translate('your_account_not_active',$this->lang->id));
                }
                Auth::login($finduser);
                if(isset($_COOKIE['a_previous_url']) && $_COOKIE['a_previous_url']!=null){
                    $previou_url=$_COOKIE['a_previous_url'];
                    @setcookie('a_previous_url',null,0, "/");
                    return redirect($previou_url);
                }

                return redirect('eg-'.$_COOKIE['locale_lang'].'/panel');

            }else{
                $is_email_exist = User::where('email', $user->email)->first();
                if($is_email_exist){
                    if($is_email_exist->is_active == 0)
                    {
                        return redirect('/login')->with('error', \App\Help::translate('your_account_not_active',$this->lang->id));
                    }
                    $is_email_exist->update([
                        'email' => $user->email,
                        'provider' => "google",
                        'is_social' => 1,
                        'provider_id'=> $user->id,
                        'provider_image'=> $user->avatar,
                    ]);
                    Auth::login($is_email_exist);
                }else{
                    if(isset($_COOKIE['parent_register_code']))
                    {
                        $parent_user=User::where('registration_code',$_COOKIE['parent_register_code'])->first();
                    }
                    $parent_id=null;
                    if(isset($parent_user))
                    {
                        $parent_id=$parent_user->id;
                    }
                    $newUser = User::create([
                        'name' => $user->name,
                        'is_active' => 1,
                        'is_social' => 1,
                        'country_id'=> $country_id,
                        'user_id'=> $parent_id,
                        'email' => $user->email,
                        'provider' => "google",
                        'provider_id' => $user->id,
                        'provider_image' => $user->avatar,
                        'password' => 'google',
                        'registration_code' => \Super::generateCode(10)
                    ]);
                    Auth::login($newUser);
                }
                if(isset($_COOKIE['a_previous_url']) && $_COOKIE['a_previous_url']!=null){
                    $previou_url=$_COOKIE['a_previous_url'];
                    @setcookie('a_previous_url',null,0, "/");
                    return redirect($previou_url);
                }
                return redirect('eg-'.$_COOKIE['locale_lang'].'/panel');
            }
        // } catch (Exception $e) {
        //     //dd($e->getMessage());
        //     echo 'error';
        // }
    }
}
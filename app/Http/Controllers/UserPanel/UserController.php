<?php

namespace App\Http\Controllers\UserPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Rules\PasswordMatch;

class UserController extends Controller
{


    public function profile(){
        $user  = Auth::user();
        if($user){
            return view('panel.profile',compact('user','data'));
        }
    }
    public function edit_profile(Request $request)
    {
        $row=auth()->user();
        return view('panel.edit_profile',compact('row'));
    }

    public function update_profile(Request $request)
    {
        $request->validate(
            [
                'name'=>'required|min:3|max:200',
                'email'=>'required|email|unique:users,email,'.auth()->user()->id,
            ]
        );
        $user = User::find(auth()->user()->id);
        $user->update($request->all());
        
        return redirect(route('panel.index'))->with('success',__('dashboard.profile_updated'));
    }

    public function change_password_form()
    {
        if(auth()->user()->is_social==1){
            return abort(404);
        }
        return view('panel.change_password');
    }

    public function change_password(Request $request)
    {
        $request->validate(
            [
                'old_password'=>['required',new PasswordMatch($request->old_password)],
                'password'=>'required|confirmed'
            ]
        );
        $user = User::find(auth()->user()->id);
        $user->password=$request->password;
        $user->save();
        return redirect(route('panel.index'))->with('success',__('dashboard.password_updated'));
    }
}

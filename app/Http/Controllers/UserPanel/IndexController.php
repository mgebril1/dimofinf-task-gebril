<?php

namespace App\Http\Controllers\UserPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EventEmailInformation;
use PDF;
class IndexController extends Controller
{

    public function index()
    {  
        return view('panel.index');
    }

}

<?php

namespace App\Http\Controllers\UserPanel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PostRepository;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
	public function __construct(PostRepository $postRepository)
	{
		$this->postRepository=$postRepository;
	}
    public function index(Request $request)
    {
    	$posts=$this->postRepository->getAll($request);
    	return view('panel.posts.index',compact('posts'));
    }

    public function create()
    {
    	return view('panel.posts.create');
    }

    public function store(PostRequest $request)
    {
    	$this->postRepository->save($request->all());
		return redirect(route('panel.posts.index'))->with('success','Post Created Successfully');    	
    }
}

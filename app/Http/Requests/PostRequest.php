<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string|min:3|max:250',
            'desc'=>'required|string|min:5|max:500',
            'contact_number'=>'required|min:9|max:11',
            'image'=>'required|image|mimes:png,jpg,jpeg|max:500000',
        ];
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Illuminate\Http\JsonResponse;

class JwtMiddleware extends BaseMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return new JsonResponse(["success" => false, "status" => 401,"data" => []  ,"message" => " Token is Invalid",
                ], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return new JsonResponse(["success" => false, "status" => 401,"data" => []  ,"message" => " Token is Expired",
                ], 401);

            }else{
                 return new JsonResponse(["success" => false, "status" => 401,"data" => []  ,"message" => " Un Authorized User",
                ], 401);

            }
        }
        return $next($request);
    }
}

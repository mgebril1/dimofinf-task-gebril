<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class PasswordMatch implements Rule
{

    public $newPassword;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($oldValue)
    {
        $this->oldValue = $oldValue;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (auth()->attempt(['email'=>auth()->user()->email,'password'=>$this->oldValue])) {
            return true;
        }

        if (Hash::check(auth()->user()->password, $this->oldValue)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Wrong password.";
    }
}

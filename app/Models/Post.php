<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Post extends Model
{
    protected $fillable=[ 'title','desc','contact_number','image','user_id'];

    public function setImageAttribute($image)
    {
    	if ($image) {
            $image_name = uploadFile($image, 'assets/images/posts/', 240, 240, 265);
            $this->attributes['image'] = 'assets/images/posts/' . $image_name;
        }
    }

    public function getImageAttribute($image)
    {
    	if ($image != null) {
    		return url($image);
    	}
    	return null;
    }

    public function user()
    {
    	return $this->belongsTo(User::class,'user_id');
    }
}

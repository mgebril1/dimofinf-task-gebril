<?php

namespace App\Exports;

 use App\Models\QRcoder;
 use App\Models\QrcodeRequest;
 use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CodeRequestExport   implements FromCollection , WithHeadings
{
    use Exportable;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $data =  QrcodeRequest::whereIn('id', $this->ids)->get();

        foreach ($data as $row) {
            $row->code =$row->code ;
            $row->url = url('/') . '/' .$row->code ;
            $row->name =$row->user->name; ;
            $row->mobile =$row->user->mobile; ;
            $row->address =$row->address ;
            unset($row->id);
            unset($row->is_active);
            unset($row->status);
            unset($row->user_id);
            unset($row->car_id);
            unset($row->created_at);
            unset($row->updated_at);
        }

        return $data;
    }
    public function headings(): array
    {
        return [
            '#',
            'Link',
            'User Name',
            'User Mobile',
            'User Address',

        ];
    }
}

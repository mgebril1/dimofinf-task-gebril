<?php

namespace App\Exports;

use App\User;
use App\Models\AffiliateHistory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class AffilatorExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $from;
    protected $to;
    public function __construct($from,$to)
    {
        $this->from=$from;
        $this->to=$to;
    }
    public function view(): View
    {
        $affilatorsIds=AffiliateHistory::where('affiliator_id','>',0)->pluck('affiliator_id')->toArray();
        
        $affilators=User::whereIn('id',$affilatorsIds);
        if (isset($this->from)) {
            $affilators=$affilators->whereDate('created_at','>=',Carbon::parse($this->from));
        }
        if (isset($this->to)) {
            $affilators=$affilators->whereDate('created_at','<=',Carbon::parse($this->to));
        }
        $affilators = $affilators->orderBy('id','desc')->get();
        return view('exports.affilators', [
            'affilators' => $affilators
        ]);
    }
}

<?php

namespace App\Exports;

use App\Models\QRcoder;
 use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class CodeExport   implements FromCollection , WithHeadings
{
    use Exportable;

    public function __construct($ids)
    {
        $this->ids = $ids;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data =  QRcoder::whereIn('id', $this->ids)->get();

        foreach ($data as $row) {
            $row->code =$row->code ;
            $row->url = url('/') . '/' .$row->code ;
            unset($row->id);
            unset($row->is_active);
            unset($row->is_used);
            unset($row->created_at);
            unset($row->updated_at);
        }

        return $data;
    }
    public function headings(): array
    {
        return [
            '#',
            'Link',

        ];
    }
}

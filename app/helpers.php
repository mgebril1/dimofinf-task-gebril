<?php  

if (!function_exists('user')) {
    function user()
    {
        return auth()->user();
    }
}

if (!function_exists('generateRandomString')) {
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (!function_exists('uploadFile')) {
    function uploadFile($file, $stringPath)
    {
        $file = $file;
        $fileName = time() . rand(10,10000) . '.' . $file->getClientOriginalExtension();
        $fileLocation = getcwd() . '/' . $stringPath . '/';
        $file->move($fileLocation, $fileName);
        if(!$file){
            return FALSE;
        }
        return $fileName;
    }
}



?>
const mix = require('laravel-mix');

mix

    .js('resources/js/vue/app.js', 'public/assets/frontend/js')

    .js('resources/js/panel/vue/app.js', 'public/assets/frontend/panel/js')

    .scripts([
        'resources/js/scripts/jquery.js',
        'resources/js/scripts/popper.min.js',
        'resources/js/scripts/bootstrap.min.js',
        'resources/js/scripts/jquery.appear.min.js',
        'resources/js/scripts/jquery.jCounter.js',
        'resources/js/scripts/jquery.magnific-popup.min.js',
        'resources/js/scripts/owl.carousel.min.js',
        'resources/js/scripts/wow.min.js',
        'resources/js/scripts/isotope.pkgd.min.js',
        'resources/js/scripts/jquery.scrollme.js',
        'resources/js/scripts/main.js',
    ], 'public/assets/frontend/js/script.js')

    .scripts([
        'resources/js/panel/scripts/vendors/js/vendor.bundle.base.js',
        'resources/js/panel/scripts/vendors/chart.js/Chart.min.js',
        'resources/js/panel/scripts/jquery.cookie.js',
        'resources/js/panel/scripts/off-canvas.js',
        'resources/js/panel/scripts/hoverable-collapse.js',
        'resources/js/panel/scripts/misc.js',
        'resources/js/panel/scripts/dashboard.js',
        'resources/js/panel/scripts/todolist.js',
        'resources/js/panel/scripts/select2.js',
        'resources/js/panel/scripts/sweetalert2.all.js',
    ], 'public/assets/frontend/panel/js/script.js')


    .copy([
        'resources/js/scripts/jquery.js',
    ], 'public/assets/frontend/panel/js/jquery.js')

    .sass('resources/sass/app.scss', 'public/assets/frontend/css')

    .sass('resources/sass/panel/scss/app.scss', 'public/assets/frontend/panel/css')
    //
    // // temp-1
    // .postCss('public/event-temp/temp-1/css-temp/animate.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/bootstrap.min.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/font-awesome.min.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/isotop.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/magnific-popup.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/owl.carousel.min.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/responsive.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/shuvo.css', 'public/event-temp/temp-1/css')
    // .postCss('public/event-temp/temp-1/css-temp/style.css', 'public/event-temp/temp-1/css')
    //
    // // temp-2
    // .postCss('public/event-temp/temp-2/css-temp/animate.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/bootstrap.min.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/font-awesome.min.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/isotop.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/magnific-popup.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/owl.carousel.min.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/responsive.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/shuvo.css', 'public/event-temp/temp-2/css')
    // .postCss('public/event-temp/temp-2/css-temp/style.css', 'public/event-temp/temp-2/css')
    //
    //
    // // temp-3
    // .postCss('public/event-temp/temp-3/css-temp/animate.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/bootstrap.min.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/font-awesome.min.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/isotop.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/magnific-popup.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/owl.carousel.min.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/responsive.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/shuvo.css', 'public/event-temp/temp-3/css')
    // .postCss('public/event-temp/temp-3/css-temp/style.css', 'public/event-temp/temp-3/css')
    //
    // // temp-4
    // .postCss('public/event-temp/temp-4/css-temp/animate.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/bootstrap.min.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/font-awesome.min.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/isotop.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/magnific-popup.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/owl.carousel.min.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/responsive.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/shuvo.css', 'public/event-temp/temp-4/css')
    // .postCss('public/event-temp/temp-4/css-temp/style.css', 'public/event-temp/temp-4/css')
    //
    //
    // // temp-5
    // .postCss('public/event-temp/temp-5/css-temp/animate.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/bootstrap.min.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/font-awesome.min.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/isotop.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/magnific-popup.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/owl.carousel.min.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/responsive.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/shuvo.css', 'public/event-temp/temp-5/css')
    // .postCss('public/event-temp/temp-5/css-temp/style.css', 'public/event-temp/temp-5/css')

    .options({
        processCssUrls: false,
        postCss: [require('postcss-rtl')],
    })

    .minify([
        'public/assets/frontend/css/app.css',
        'public/assets/frontend/js/manifest.js',
        'public/assets/frontend/js/app.js',
        'public/assets/frontend/js/script.js',
        'public/assets/frontend/js/vendor.js',
        'public/assets/frontend/panel/css/app.css',
        'public/assets/frontend/panel/js/script.js',
        'public/assets/frontend/panel/js/manifest.js',
        'public/assets/frontend/panel/js/app.js',
        'public/assets/frontend/panel/js/vendor.js',
        'public/assets/frontend/panel/js/jquery.js'
    ])

    .version([
        'public/assets/frontend/css/app.css',
        'public/assets/frontend/js/manifest.js',
        'public/assets/frontend/js/app.js',
        'public/assets/frontend/js/script.js',
        'public/assets/frontend/js/vendor.js',
        'public/assets/frontend/panel/css/app.css',
        'public/assets/frontend/panel/js/script.js',
        'public/assets/frontend/panel/js/manifest.js',
        'public/assets/frontend/panel/js/app.js',
        'public/assets/frontend/panel/js/vendor.js',
        'public/assets/frontend/panel/js/jquery.js'
    ])

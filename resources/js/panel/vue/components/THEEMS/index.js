export default [
    {
        file: require('./THEEM_4.js').default,
        img: '/event-temp/temp-4/image.jpg',
        id: 'THEEM_4',
        requiredSection: ['basic', 'description', 'footer'],
        color: '#13dc38',
        sections: [
            {
                name: `basic`,
                order: 1,
                img: `/event-temp/temp-4/sections/basic.png`,
                id: `basic`
            },{
                name: `description`,
                order: 2,
                img: `/event-temp/temp-4/sections/description.png`,
                id: `description`
            },{
                name: `counter`,
                order: 3,
                img: `/event-temp/temp-4/sections/counter.png`,
                id: `counter`,
                hide: true
            },{
                name: `speakers`,
                order: 4,
                img: `/event-temp/temp-4/sections/speakers.png`,
                id: `speakers`
            },{
                name: `video`,
                order: 5,
                img: `/event-temp/temp-4/sections/video.png`,
                id: `video`
            },{
                name: `gallery`,
                order: 6,
                img: `/event-temp/temp-4/sections/gallery.png`,
                id: `gallery`
            },{
                name: `sponsors`,
                order: 7,
                img: `/event-temp/temp-4/sections/sponsors.png`,
                id: `sponsors`
            },{
                name: `comments`,
                order: 8,
                img: `/event-temp/temp-4/sections/comments.png`,
                id: `comments`
            },{
                name: `banner`,
                order: 9,
                img: `/event-temp/temp-4/sections/banner.png`,
                id: `banner`
            },{
                name: `footer`,
                order: 10,
                img: `/event-temp/temp-4/sections/footer.png`,
                id: `footer`
            }

        ],
    },
    {
        file: require('./THEEM_1.js').default,
        img: '/event-temp/temp-1/image.jpg',
        id: 'THEEM_1',
        requiredSection: ['basic', 'description', 'footer'],
        color: '#13dc38',
        sections: [
            {
                name: `basic`,
                order: 1,
                img: `/event-temp/temp-1/sections/basic.png`,
                id: `basic`
            },{
                name: `description`,
                order: 2,
                img: `/event-temp/temp-1/sections/description.png`,
                id: `description`
            },{
                name: `counter`,
                order: 3,
                img: `/event-temp/temp-1/sections/counter.png`,
                id: `counter`
            },{
                name: `speakers`,
                order: 4,
                img: `/event-temp/temp-1/sections/speakers.png`,
                id: `speakers`
            },{
                name: `video`,
                order: 5,
                img: `/event-temp/temp-1/sections/video.png`,
                id: `video`
            },{
                name: `gallery`,
                order: 6,
                img: `/event-temp/temp-1/sections/gallery.png`,
                id: `gallery`
            },{
                name: `sponsors`,
                order: 7,
                img: `/event-temp/temp-1/sections/sponsors.png`,
                id: `sponsors`
            },{
                name: `comments`,
                order: 8,
                img: `/event-temp/temp-1/sections/comments.png`,
                id: `comments`
            },{
                name: `banner`,
                order: 9,
                img: `/event-temp/temp-1/sections/banner.png`,
                id: `banner`
            },{
                name: `footer`,
                order: 10,
                img: `/event-temp/temp-1/sections/footer.png`,
                id: `footer`
            }

        ],
    },
    {
        file: require('./THEEM_2.js').default,
        img: '/event-temp/temp-2/image.jpg',
        id: 'THEEM_2',
        requiredSection: ['basic', 'description', 'footer'],
        color: '#13dc38',
        sections: [
            {
                name: `basic`,
                order: 1,
                img: `/event-temp/temp-2/sections/basic.png`,
                id: `basic`
            },{
                name: `description`,
                order: 2,
                img: `/event-temp/temp-2/sections/description.png`,
                id: `description`
            },{
                name: `counter`,
                order: 3,
                img: `/event-temp/temp-2/sections/counter.png`,
                id: `counter`,
                hide: true
            },{
                name: `speakers`,
                order: 4,
                img: `/event-temp/temp-2/sections/speakers.png`,
                id: `speakers`
            },{
                name: `video`,
                order: 5,
                img: `/event-temp/temp-2/sections/video.png`,
                id: `video`
            },{
                name: `gallery`,
                order: 6,
                img: `/event-temp/temp-2/sections/gallery.png`,
                id: `gallery`
            },{
                name: `sponsors`,
                order: 7,
                img: `/event-temp/temp-2/sections/sponsors.png`,
                id: `sponsors`
            },{
                name: `comments`,
                order: 8,
                img: `/event-temp/temp-2/sections/comments.png`,
                id: `comments`
            },{
                name: `banner`,
                order: 9,
                img: `/event-temp/temp-2/sections/banner.png`,
                id: `banner`
            },{
                name: `footer`,
                order: 10,
                img: `/event-temp/temp-2/sections/footer.png`,
                id: `footer`
            }

        ],
    },
    {
        file: require('./THEEM_3.js').default,
        img: '/event-temp/temp-3/image.jpg',
        id: 'THEEM_3',
        requiredSection: ['basic', 'description', 'footer'],
        color: '#13dc38',
        sections: [
            {
                name: `basic`,
                order: 1,
                img: `/event-temp/temp-3/sections/basic.png`,
                id: `basic`
            },{
                name: `description`,
                order: 2,
                img: `/event-temp/temp-3/sections/description.png`,
                id: `description`
            },{
                name: `counter`,
                order: 3,
                img: `/event-temp/temp-3/sections/counter.png`,
                id: `counter`,
                hide: true
            },{
                name: `speakers`,
                order: 4,
                img: `/event-temp/temp-3/sections/speakers.png`,
                id: `speakers`
            },{
                name: `video`,
                order: 5,
                img: `/event-temp/temp-3/sections/video.png`,
                id: `video`
            },{
                name: `gallery`,
                order: 6,
                img: `/event-temp/temp-3/sections/gallery.png`,
                id: `gallery`
            },{
                name: `sponsors`,
                order: 7,
                img: `/event-temp/temp-3/sections/sponsors.png`,
                id: `sponsors`
            },{
                name: `comments`,
                order: 8,
                img: `/event-temp/temp-3/sections/comments.png`,
                id: `comments`
            },{
                name: `banner`,
                order: 9,
                img: `/event-temp/temp-3/sections/banner.png`,
                id: `banner`
            },{
                name: `footer`,
                order: 10,
                img: `/event-temp/temp-3/sections/footer.png`,
                id: `footer`
            }

        ],
    },
    {
        file: require('./THEEM_5.js').default,
        img: '/event-temp/temp-5/image.jpg',
        id: 'THEEM_5',
        requiredSection: ['basic', 'description', 'footer'],
        color: '#13dc38',
        sections: [
            {
                name: `basic`,
                order: 1,
                img: `/event-temp/temp-5/sections/basic.png`,
                id: `basic`
            },{
                name: `description`,
                order: 2,
                img: `/event-temp/temp-5/sections/description.png`,
                id: `description`
            },{
                name: `counter`,
                order: 3,
                img: `/event-temp/temp-5/sections/counter.png`,
                id: `counter`,
                hide: true
            },{
                name: `speakers`,
                order: 4,
                img: `/event-temp/temp-5/sections/speakers.png`,
                id: `speakers`
            },{
                name: `video`,
                order: 5,
                img: `/event-temp/temp-5/sections/video.png`,
                id: `video`
            },{
                name: `gallery`,
                order: 6,
                img: `/event-temp/temp-5/sections/gallery.png`,
                id: `gallery`
            },{
                name: `sponsors`,
                order: 7,
                img: `/event-temp/temp-5/sections/sponsors.png`,
                id: `sponsors`
            },{
                name: `comments`,
                order: 8,
                img: `/event-temp/temp-5/sections/comments.png`,
                id: `comments`
            },{
                name: `banner`,
                order: 9,
                img: `/event-temp/temp-5/sections/banner.png`,
                id: `banner`
            },{
                name: `footer`,
                order: 10,
                img: `/event-temp/temp-5/sections/footer.png`,
                id: `footer`
            }

        ],
    },
]
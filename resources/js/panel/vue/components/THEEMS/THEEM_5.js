import {getDefaultText} from "./getDefaultText";
const basic = (editable, isRequired, data = {}, {basic}, lang, invitation) => {
  return `
    <section class="hero-area content-left">
         <div class="banner-item" 
         id="basic-section-bg" 
         v-if="sections.some((i) => i === 'basic') || ${isRequired}"
         style="background-image: url(${basic['banner-bg'] ? basic['banner-bg'] : ''});"
         >
         
       ${editable ? `<image-panel
            v-bind:position="['basic', 'banner-bg']"
            v-bind:is-bg="true"
            v-bind:element-id="'basic-section-bg'"
            v-bind:url="'${basic['banner-bg'] ? basic['banner-bg'] : ''}'"
            ></image-panel>
        `: ''
        }
            <div class="container">
               <div class="row">
                  <div class="col-lg-5">
                     <div class="banner-content-wrap">
                        <img class="title-shap-img" src="/event-temp/temp-5/images/shap/title-white.png" alt="">
                        
                        ${editable ?
                              `<text-editor 
                                    v-bind:position="['basic', 'banner-title']" 
                                    v-bind:tag="'h1'" 
                                    v-bind:class-name="'banner-title wow fadeInUp'" 
                                    v-bind:value-text="'${basic['banner-title']}'" 
                                    v-bind:default-text="'write text here'"></text-editor>`
                              : `<h1 class="banner-title wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="700ms"> ${basic['banner-sub-title']} </h1>`
                          }
                        
                           ${editable && data ?
                              `<p class="banner-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms"> 
                                   <date-time v-bind:position="['basic', 'start-date']" 
                                        v-bind:date="'${data?.start_date}'" 
                                        v-bind:time="'${data?.start_date}'">
                                    </date-time>
                               </p>`
                              : `<p class="banner-info wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="500ms">${basic['start-date']}</p>`
                          }
      
                            <div class="banner-btn wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="800ms">
                            
                            <attended-btn v-slot="slotProps" eventDate='${JSON.stringify(data)}'>
                             <a @click.prevent="() => {}" class="btn" :class="slotProps.btnClass" style="display: none">
                                  ${getDefaultText(lang, 'Attended')}
                                  <ul class="attended-dropdown-content">
                                <li @click.prevent="slotProps.changeAttend('going')">${getDefaultText(lang, 'Going')}</li>
                                <li @click.prevent="slotProps.changeAttend('maybe')">${getDefaultText(lang, 'Maybe')}</li>
                                <li @click.prevent="slotProps.changeAttend('not_going')">${getDefaultText(lang, 'Not Going')}</li>
                                  </ul>
                              </a>
                              
                             <a style="display: none" @click.prevent="slotProps.downloadTicket" class="btn download-ticket-btn" v-if="(slotProps.attendVal === 'going' || slotProps.attendVal === 'maybe') && slotProps.invitation">
                                    <i class="fa fa-download"></i> ${getDefaultText(lang, 'Download Ticket')}
                                  </a>
                            </attended-btn>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
`
}

const description = (editable, isRequired, data = {}, {description}) => {
  return `
      <section v-if="sections.some((i) => i === 'description') || ${isRequired}" class="business-event-area ts-funfact">
         <div class="container">
            <div class="row mb-3">
               <div class="col-lg-12 col-md-12">
                  <div class="title-area">
                    ${editable ?
                          `<text-editor
                                               v-bind:position="['description', 'section-heading']"
                                               v-bind:tag="'h1'"
                                               v-bind:class-name="'section-heading white'"
                                                v-bind:value-text="'${description['section-heading']}'" 
                                                   v-bind:default-text="'write text here'"
                                            >
                                            </text-editor>`
                          : `<h1 class="section-heading white"> ${description['section-heading']} </h1>`
                      }
                                        
                       ${editable ?
                          `<text-editor 
                                                v-bind:position="['description', 'desc']" 
                                                v-bind:tag="'p'"
                                                 v-bind:class-name="'white'"
                                                v-bind:value-text="'${description['desc']}'" 
                                            v-bind:default-text="'write text here'"
                                              ></text-editor>`
                          : `<p class="white">${description['desc']}</p>`
                      }
                  </div>
               </div>
             
            </div>
         </div>
        <div class="container">
            <div class="row mt-2">
              <div class="ts-map" style="width: 100%;">
                 <div class="mapouter">
                    <div class="gmap_canvas" style="width: 100%;border-radius: 0;height: 200px;">
                   <div style="position: relative;display: flex;align-items: center;justify-content: center;">
                            <img src="/assets/map-temp.png" alt="map-temp" />
                         <a class="map-btn" href="${data?.location_url}" target="_blank">
                                <i class="fa fa-map"></i>
                                ${words['Open Event Location']}
                            </a>
                        </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
      </section>    
`
}

const footer = (editable, isRequired, data = {}, words, lang, link, logo, footerText, footerHeading) => {
    return `
    <footer class="ts-footer ts-footer-item pt-3 pb-3">
            <div class="container">
               <div class="row footer-border">
                  <div class="col-lg-12">
                    <div style="flex-direction: row;display: flex;justify-content: center; margin-bottom: 8px;align-items: center;">
                         <a target="_blank" href="${editable ? 'javascript:void(0)' :  link }" style="padding: 0 10px; display: block;">
                            <img style="width: 68px; margin: auto; display: block;filter: brightness(2.5);" src="${logo}" alt="logo" />
                        </a>     
                        <p style="text-align: ${lang === 'en' ? 'left' : 'right'};color: #efefef;margin: 4px 7px; max-width: 400px;">${footerText}</p>
                    </div>
                  <div class="copyright-text text-center">
                     <p>© ${new Date().getFullYear()}, <a href="${link}">Eventiqr</a> ${getDefaultText(lang, 'All rights reserved')}.</p>
                  </div>
                  </div>
               </div>
            </div>
         </footer>
    `
}

const comments = (editable, isRequired, data = {}, {comments}) => {
  return `
        <section v-if="sections.some((i) => i === 'comments') || ${isRequired}" class="business-conference" style="padding-top: 90px; background: url(\/event-temp/temp-4/images/bg/feature_bg.png\);">
             <div class="container">
                <div class="row">
                   <div class="col-md-10 mx-auto">
                      <div class="section-title-area pb-0">
                      
                      ${editable ?
                          `<text-editor 
                            v-bind:position="['comments', 'sub-title']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'white sub-title'" 
                            v-bind:value-text="'${comments['sub-title']}'" 
                                v-bind:default-text="'Leave a Comment'"></text-editor>`
                          : `<span class="white sub-title">${comments['sub-title'] || `Comments`}</span>`
                      }
                                          
                      ${editable ?
                          `<text-editor 
                            v-bind:position="['comments', 'section-heading']" 
                            v-bind:tag="'h2'" 
                            v-bind:class-name="'white section-heading pb-0'" 
                            v-bind:value-text="'${comments['section-heading']}'" 
                            v-bind:default-text="'Comments'"></text-editor>`
                          : `<h2 class="white section-heading pb-0">${comments['section-heading'] || `Comments`}</h2>`
                      }
                      </div>
                   </div>
                </div>
                <div class="row">
                    <comments v-bind:id="${data.id}" />
                </div>
            </div>
        </section>
    `
}

const speakers = (editable, isRequired, data = {}, {speakers}, lang) => {
  return `

      <section 
        class="ts-speakers"
        id="speakers-section" v-if="sections.some((i) => i === 'speakers') || ${isRequired}"
        >
         <div class="container">
            <div class="row">
               <div class="col-lg-8 mx-auto">
                   ${editable ?
                      `<text-editor 
                            v-bind:position="['speakers', 'title']" 
                            v-bind:tag="'h2'" 
                            v-bind:class-name="'section-title white text-center'" 
                            v-bind:value-text="'${speakers['title']}'" 
                            v-bind:default-text="'Speakers'"></text-editor>`
                      : `<h2 class="section-title white text-center">${speakers['title']}</h2>`
                  }
               </div>
            </div>
            
                <loop-item
                    v-bind:editable="${editable}"
                    v-bind:default-list='${speakers['speakers'] ? JSON.stringify(speakers['speakers']) : '{}' }'
                    v-bind:position="['speakers', 'speakers']" 
                    v-bind:parent-id="'speakers-section'"
                    v-bind:class-name="'row'"
                    v-bind:inputs="[{label: '${getDefaultText(lang, 'Name')}', name: 'name', type: 'text'}, {label: '${getDefaultText(lang, 'Title')}',name: 'title', type: 'text'}, {label: '${getDefaultText(lang, 'Avatar')}',name: 'img', type: 'img'}]"
                    v-slot="slotProps" 
                    >
                    
                <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="400ms">
                  <div class="ts-speaker white-text">
                     <div class="speaker-img">
                        <img class="img-fluid" :src="slotProps.content.img || 'https://via.placeholder.com/400x400'" alt="">
                     </div>
                     <div class="ts-speaker-info">
                        <h3 class="ts-title"><a href="#">{{slotProps.content.name || 'Name'}}</a></h3>
                        <p>
                         {{slotProps.content.title || 'Title'}}
                        </p>
                     </div>
                  </div>
               </div> 
            </loop-item>
         </div>

         
         <div class="speaker-shap">
            <img class="shap1" src="images/shap/home_speaker_memphis1.png" alt="">
            <img class="shap2" src="images/shap/home_speaker_memphis2.png" alt="">
         </div>
         
      </section>

`
}

const sponsors = (editable, isRequired, data = {}, {sponsors}) => {
    return `

      <section class="ts-sponsors sponsors-border" 
        v-if="sections.some((i) => i === 'sponsors') || ${isRequired}" 
        id="sponsors-section"
        >
         <div class="container">
             <images-uploader
                v-bind:editable="${editable}"
                v-bind:default-list="'${sponsors['logos'] || []}'"
                v-bind:parent-id="'sponsors-section'" 
                v-bind:position="['sponsors', 'logos']"
                v-bind:min="3" 
                v-bind:max="6" 
                v-bind:class-name="'row sponsors-wrap'" 
                v-slot="slotProps" >
                <div class="col-md mb-1">
                     <a href="#" class="sponsors-logo">
                      ${editable ?
                            `<a-button @click="slotProps.deleteItem(slotProps.index)" class="images-uploader-delete-btn"> <a-icon type="delete" /> </a-button>` : ''
                        }
                       <img class="img-fluid" :src="slotProps.content || 'https://via.placeholder.com/135x36'" alt="">
                      </a>
                  </div>
              </images-uploader>
         </div>
      </section>
`
}

const gallery = (editable, isRequired, data = {}, {gallery}) => {
  return `

      <section class="ts-gallery p-60" v-if="sections.some((i) => i === 'gallery') || ${isRequired}" id="gallery-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 mx-auto">
                ${editable ?
                  `<text-editor 
                    v-bind:position="['gallery', 'title']" 
                    v-bind:tag="'h2'" 
                    v-bind:class-name="'section-title white'" 
                    v-bind:value-text="'${gallery['title']}'" 
                                v-bind:default-text="'Gallery'"></text-editor>`
                  : `<h2 class="section-title white">${gallery['title']}</h2>`
              }
               </div>
            </div>
            
            <images-uploader
                v-bind:editable="${editable}"
                v-bind:default-list="'${gallery['images'] || []}'"
                v-bind:parent-id="'gallery-section'" 
                v-bind:position="['gallery', 'images']"
                v-bind:min="3" 
                v-bind:max="6" 
                v-bind:class-name="'row gallery-wrap'" 
                v-slot="slotProps" >
               <div class="col-lg-3 mb-3">
                   ${editable ?
                      `<a-button @click="slotProps.deleteItem(slotProps.index)" class="images-uploader-delete-btn">
                          <a-icon type="delete" />
                        </a-button>` : ''
                   }
                   
                  <a href="javascript:void(0)" class="ts-popup" :href="slotProps.content || 'https://via.placeholder.com/135x36'">
                     <img class="img-fluid gallery-1"  :src="slotProps.content || 'https://via.placeholder.com/135x36'" :alt="slotProps.content || 'https://via.placeholder.com/135x36'">
                  </a>
               </div>
           </images-uploader>
           
            
         </div>
         <div class="speaker-shap">
            <img class="shap1" src="images/shap/home_schedule_memphis2.png" alt="">
         </div>
      </section>
      
    `
}

const banner = (editable, isRequired, data = {}, {banner}) => {
    return `

<section class="ts-book-seat" id="banner-section-bg" v-if="sections.some((i) => i === 'banner') || ${isRequired}" 
    style="background-image: url(${banner['banner-bg'] ? banner['banner-bg'] : ''});">
            ${editable ? `<image-panel
                    v-bind:position="['banner', 'banner-bg']"
                    v-bind:is-bg="true"
                    v-bind:element-id="'banner-section-bg'"
                    v-bind:url="'${banner['banner-bg'] ? banner['banner-bg'] : ''}'"
                    ></image-panel>
                    `: ''
    }
    
    <div class="container">
       <div class="row">
          <div class="col-lg-8 mx-auto">
             <div class="book-seat-content text-center mb-100">
             
               ${editable ?
        `<text-editor 
                                    v-bind:position="['banner', 'title']" 
                                    v-bind:tag="'h2'" 
                                    v-bind:class-name="'section-title white'" 
                                    v-bind:value-text="'${banner['title']}'" 
                        v-bind:default-text="'Banner Title'"></text-editor>`
        : `<h2 class="section-title white">${banner['title']}</h2>`
    }
                               
             </div>
          </div>
       </div>
    </div>
 </section>
    `
}

const counter = (editable, isRequired, data = {}, {counter}) => {
    return `
<section class="ts-funfact" id="counter-section" v-if="sections.some((i) => i === 'counter') || ${isRequired}" 
    style="background-image: url(${counter['counter-bg'] ? counter['counter-bg'] : ''});">
         ${editable ? `<image-panel
            v-bind:position="['counter', 'counter-bg']"
            v-bind:is-bg="true"
            v-bind:element-id="'counter-section'"
            v-bind:url="'${counter['counter-bg'] ? counter['counter-bg'] : ''}'"
            ></image-panel>
            `: ''
    }
        
         <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="ts-single-funfact">
                        <h3 class="funfact-num">
                        ${editable ?
        `<number-editor
                                v-bind:position="['counter', '0-number']" 
                                v-bind:tag="'span'" 
                                v-bind:class-name="'counterUp'"
                                v-bind:default-text="'${counter['0-number'] || '300'}'"></number-editor>`
        : `<span class="counterUp" data-counter="${counter['0-number'] || '300'}">${counter['0-number'] || '300'}</span>`
    }
                        </h3>
                        ${editable ?
        `<text-editor 
                                v-bind:position="['counter', '0-text']" 
                                v-bind:tag="'h4'" 
                                v-bind:class-name="'funfact-title'" 
                                v-bind:value-text="'${counter['0-text']}'" 
                                v-bind:default-text="'Text'"></text-editor>`
        : `<h4 class="funfact-title">${counter['0-text']}</h4>`
    }
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="ts-single-funfact">
                        <h3 class="funfact-num">
                            ${editable ?
        `<number-editor 
                            v-bind:position="['counter', '1-number']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'counterUp'" 
                            v-bind:default-text="'${counter['1-number'] || '300'}'"></number-editor>`
        : `<span class="counterUp" data-counter="${counter['1-number'] || '300'}">${counter['1-number'] || '300'}</span>`
    }
                        </h3>
                        ${editable ?
        `<text-editor 
                        v-bind:position="['counter', '1-text']" 
                        v-bind:tag="'h4'" 
                        v-bind:class-name="'funfact-title'"
                        v-bind:value-text="'${counter['1-text']}'" 
                        v-bind:default-text="'Text'"></text-editor>`
        : `<h4 class="funfact-title">${counter['1-text']}</h4>`
    }
                    </div>
                </div>              
                <div class="col-lg-3 col-md-6">
                    <div class="ts-single-funfact">
                        <h3 class="funfact-num">
                            ${editable ?
        `<number-editor
                            v-bind:position="['counter', '2-number']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'counterUp'" 
                            v-bind:default-text="'${counter['2-number'] || '300'}'"></number-editor>`
        : `<span class="counterUp" data-counter="${counter['2-number'] || '300'}">${counter['2-number'] || '300'}</span>`
    }
                        </h3>
                        ${editable ?
        `<text-editor 
                        v-bind:position="['counter', '2-text']" 
                        v-bind:tag="'h4'" 
                        v-bind:class-name="'funfact-title'" 
                        v-bind:value-text="'${counter['2-text']}'" 
                        v-bind:default-text="'Text'"></text-editor>`
        : `<h4 class="funfact-title">${counter['2-text']}</h4>`
    }
                    </div>
                </div> 
                <div class="col-lg-3 col-md-6">
                    <div class="ts-single-funfact">
                        <h3 class="funfact-num">
                        ${editable ?
        `<number-editor
                        v-bind:position="['counter', '3-number']" 
                        v-bind:tag="'span'" 
                        v-bind:class-name="'counterUp'" 
                        v-bind:default-text="'${counter['3-number'] || '300'}'"></number-editor>`
        : `<span class="counterUp" data-counter="${counter['3-number'] || '300'}">${counter['3-number'] || '300'}</span>`
    }
                        </h3>
                        ${editable ?
        `<text-editor 
                        v-bind:position="['counter', '3-text']" 
                        v-bind:tag="'h4'" 
                        v-bind:class-name="'funfact-title'" 
                        v-bind:value-text="'${counter['3-text']}'" 
                        v-bind:default-text="'Text'"></text-editor>`
        : `<h4 class="funfact-title">${counter['3-text']}</h4>`
    }
                    </div>
                </div>
            </div>
         </div>
      </section>
`
}

const countdown = (editable, isRequired, data = {countdown}) => {
  return ''
  return `
        <div v-if="sections.some((i) => i === 'countdown') || ${isRequired}">countdown</div>
    `
}

const video = (editable, isRequired, data = {}, {video}) => {
  return `
      <section v-if="sections.some((i) => i === 'video') || ${isRequired}" class="main-container pb-0" id="main-container">
       <div class="container">
        <div class="row">
         <div class="col-lg-10 mx-auto">
          <div class="blog-details">
           <div class="entry-header">
           ${editable ?
              `<text-editor 
                v-bind:position="['video', 'title']" 
                v-bind:tag="'h2'" 
                v-bind:class-name="'entry-title white text-center'" 
                v-bind:value-text="'${video['title']}'" 
                        v-bind:default-text="'Video'"></text-editor>`
              : `<h2 class="entry-title white text-center">${video['title']}</h2>`
          }
           </div>
           <div class="post-video post-media post-image text-center mt-5 mb-5" style="max-width: 100%;width: 100%;min-height: 530px;">
           
           ${video?.iframe ?
      `<iframe 
                    style="max-width: 100%;width: 100%;min-height: 530px;" 
                    src="${video?.iframe}" 
                    title="YouTube video player" 
                    frameborder="0" 
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                    allowfullscreen
                >
                </iframe>`
      : `<img class="img-fluid" src="https://via.placeholder.com/800x500?text=video" alt="">`
  }
         
            ${editable ?
      `<video-input v-bind:position="['video', 'iframe']" v-bind:value="'${video?.iframe}'"  />`
      : ``
  }
                
           </div>
          </div>
         </div>
        </div>
       </div>
      </section>
    `
}

const temp = (editable, {requiredSection, color, data = {}, content, language, words, invitation, link, logo, footerText, footerHeading, csrf}) => {
  return `
<!DOCTYPE html>
<html data-token="${csrf}" lang="${language?.code || 'en'}" data-category-id="${data?.category_id}" dir="${language?.direction || 'ltr'}" data-theme-color="${color || '#13dc38'}" data-event-id="${data.id}" data-invitation-code="${invitation?.data?.invitation?.code}">

<head><!DOCTYPE html>
<html lang="en">

<head>
   
   <meta charset="utf-8">

   
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
   
   <title>Eventiqr - ${data?.name}</title>
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/bootstrap.min.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/font-awesome.min.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/animate.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/magnific-popup.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/owl.carousel.min.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/isotop.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/xsIcon.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/style.css">
   
   <link rel="stylesheet" href="/event-temp/temp-5/css/responsive.css">

    ${editable ? `
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.22.1/css/medium-editor.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.22.1/css/themes/flat.css">
    ` : ''}
    
            <style>
                ${
                      color ? `
                      .body-color {
                            background: ${color} !important;
                        }
                        ` : ``
                  }
                
                            ${`
                .banner-btn .btn.download-ticket-btn{
                    color: ${color || '#13dc38'} !important;
                }
                .banner-btn .btn.download-ticket-btn i{
                    color: ${color || '#13dc38'} !important;
                }
                .banner-btn .btn.attended-dropdown:hover{
                    background: ${color || '#13dc38'} !important;
                    border-color: ${color || '#13dc38'} !important;
                }
                .btn{
                    background: ${color || '#13dc38'} !important;
                    border-color: ${color || '#13dc38'} !important;
                }
            `}
                            
                .copyright-text a, a.copyright-text, .title-normal{
                    color: #ffffff !important;
                }
                          
                .white, .banner-title *{
                    color: #fff !important;
                }
                .banner-title div p{
                font-size: inherit;
                 margin: 0;
                }
                
             .download-ticket-btn{
                margin: 5px 0 !important;
            }
            
                    .map-btn {
          background: #55d934;
          background-image: -webkit-linear-gradient(top, #55d934, #55d934);
          background-image: -moz-linear-gradient(top, #55d934, #55d934);
          background-image: -ms-linear-gradient(top, #55d934, #55d934);
          background-image: -o-linear-gradient(top, #55d934, #55d934);
          background-image: linear-gradient(to bottom, #55d934, #55d934);
          border-radius: 28px;
          color: #ffffff !important;
          font-size: 20px;
          padding: 10px 20px 10px 20px;
          text-decoration: none;
          position: absolute;
        }
        
        .map-btn:hover {
          background: #55d934;
          background-image: -webkit-linear-gradient(top, #55d934, #55d934);
          background-image: -moz-linear-gradient(top, #55d934, #55d934);
          background-image: -ms-linear-gradient(top, #55d934, #55d934);
          background-image: -o-linear-gradient(top, #55d934, #55d934);
          background-image: linear-gradient(to bottom, #55d934, #55d934);
          color: #ffffff !important;
        }

            </style>
   
   
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      

</head>

<body class="body-color">
   <div class="body-inner" id="eventiqr-panel">
            
      ${content.basic ? basic(editable, requiredSection.some((i) => i === 'basic'), data, content, language?.code, invitation) : ''}
      ${content.description ? description(editable, requiredSection.some((i) => i === 'description'), data , content, language?.code) : ''}
      ${content.sponsors ? sponsors(editable, requiredSection.some((i) => i === 'sponsors'), data, content, language?.code) : ''}
      ${content.speakers ? speakers(editable, requiredSection.some((i) => i === 'speakers'), data, content, language?.code) : ''}
      ${content.video ? video(editable, requiredSection.some((i) => i === 'video'), data, content, language?.code) : ''}
      ${content.comments ? comments(editable, requiredSection.some((i) => i === 'comments'), data, content, language?.code) : ''}
      ${content.gallery ? gallery(editable, requiredSection.some((i) => i === 'gallery'), data, content, language?.code) : ''}

      
      <div class="footer-area">
            ${footer(editable, requiredSection.some((i) => i === 'footer'), data, words, language?.code, link, logo, footerText, footerHeading)}
         <div class="BackTo">
            <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
         </div>
      </div>

   </div>
   
                   <script>
                    window.words = ${JSON.stringify(words)}
                </script>
                
      <script src="/event-temp/temp-5/js/jquery.js"></script>

      <script src="/event-temp/temp-5/js/popper.min.js"></script>
      
      <script src="/event-temp/temp-5/js/bootstrap.min.js"></script>
      
      <script src="/event-temp/temp-5/js/jquery.appear.min.js"></script>
      
      <script src="/event-temp/temp-5/js/jquery.jCounter.js"></script>
      
      <script src="/event-temp/temp-5/js/jquery.magnific-popup.min.js"></script>
      
      <script src="/event-temp/temp-5/js/owl.carousel.min.js"></script>
      
      <script src="/event-temp/temp-5/js/wow.min.js"></script>
      
      <script src="/event-temp/temp-5/js/isotope.pkgd.min.js"></script>
      
      <script src="/event-temp/temp-5/js/main.js"></script>
      
      <script src="/assets/frontend/panel/js/manifest.min.js"></script>
      <script src="/assets/frontend/panel/js/vendor.min.js"></script>
      <script src="/assets/frontend/panel/js/script.min.js"></script>
      <script src="/assets/frontend/panel/js/app.min.js"></script>
</body>

</html>
  `
}

export default temp
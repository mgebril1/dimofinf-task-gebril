export const getDefaultText = (lang, key) => {
    const words = {
        en: {
            'Attended': 'Attended',
            Going: 'Going',
            Maybe: 'Maybe',
            'Not Going': 'Not Going',
            Name: 'Name',
            Title: 'Title',
            Avatar: 'Avatar',
            'All rights reserved': 'All rights reserved',
            'Download Ticket': 'Download Invitation'
        },
        ar: {
            'Attended': 'تسجيل الحضور',
            Going: 'ذاهب',
            Maybe: 'يمكن',
            'Not Going': 'لست ذاهبا',
            Name: 'الاسم',
            Title: 'الوظيفة',
            Avatar: 'الصورة',
            'All rights reserved': 'جميع الحقوق مخفوظه',
            'Download Ticket': 'تنزيل الدعوة'
        }
    }
    return words[lang][key]
}
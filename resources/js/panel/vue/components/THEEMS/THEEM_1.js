import {getDefaultText} from "./getDefaultText";

const basic = (editable, isRequired, data = {}, {basic}, words, lang, invitation) => {
    return `
      <section id="basic-section-bg" v-if="sections.some((i) => i === 'basic') || ${isRequired}" 
        class="hero-area" 
        style="background-image: url(${basic['banner-bg'] ? basic['banner-bg'] : ''});"
      >
      
        ${editable ? `<image-panel
            v-bind:position="['basic', 'banner-bg']"
            v-bind:is-bg="true"
            v-bind:element-id="'basic-section-bg'"
            v-bind:url="'${basic['banner-bg'] ? basic['banner-bg'] : ''}'"
            ></image-panel>
            `: ''
        }
        
         <div class="hero-content-wrapper">
            <div class="hero-content">
               <div class="title-section-area">
               ${editable ? 
                    `<text-editor 
                        v-bind:position="['basic', 'banner-title']" 
                        v-bind:tag="'h1'" 
                        v-bind:class-name="'banner-title'" 
                        v-bind:value-text="'${basic['banner-title']}'" 
                        v-bind:default-text="'write text here'"></text-editor>`
                    : `<h1 class="banner-title"> ${basic['banner-title']} </h1>`
                }
                  
               ${editable ?
                    `<text-editor v-bind:position="['basic', 'banner-sub-title']" v-bind:tag="'p'" 
                    v-bind:value-text="'${basic['banner-sub-title']}'" 
                                v-bind:default-text="'write text here'"></text-editor>`
                    : `<p>${basic['banner-sub-title']}</p>`
                }
                  
               </div>
               <div class="button-and-social-area">
                   <attended-btn v-slot="slotProps" eventDate='${JSON.stringify(data)}'>
                     <a @click.prevent="() => {}" class="register-btn btn" :class="slotProps.btnClass" style="display: none">
                           ${getDefaultText(lang, 'Attended')} 
                          <ul class="attended-dropdown-content">
                            <li @click.prevent="slotProps.changeAttend('going')">${getDefaultText(lang, 'Going')}</li>
                            <li @click.prevent="slotProps.changeAttend('maybe')">${getDefaultText(lang, 'Maybe')}</li>
                            <li @click.prevent="slotProps.changeAttend('not_going')">${getDefaultText(lang, 'Not Going')}</li>
                          </ul>
                      </a>
                      
                    <a style="display: none" @click.prevent="slotProps.downloadTicket" class="btn download-ticket-btn" v-if="(slotProps.attendVal === 'going' || slotProps.attendVal === 'maybe') && slotProps.invitation">
                        <i class="fa fa-download"></i> ${getDefaultText(lang, 'Download Ticket')}
                      </a>
                    </attended-btn>
               </div>
               <div class="banner-info-meta">
                  <ul>
                     <li>
                        <p>
                            <i aria-hidden="true" class="far fa-calendar-alt"></i> 
                            ${editable && data ?
                                `<date-time v-bind:position="['basic', 'start-date']" 
                                    v-bind:date="'${data?.start_date}'" 
                                    v-bind:time="'${data?.start_date}'">
                                </date-time>` : `${basic['start-date']}`
                            }
                        </p>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
`
}

const description = (editable, isRequired, data = {}, {description}) => {


    return `
      <section v-if="sections.some((i) => i === 'description') || ${isRequired}" class="business-event-area ts-funfact">
         <div class="container">
            <div class="row mb-3">
               <div class="col-lg-12 col-md-12">
                  <div>
                    ${editable ?
                       `<text-editor
                           v-bind:position="['description', 'section-heading']"
                           v-bind:tag="'h1'"
                           v-bind:class-name="'section-heading'"
                           v-bind:value-text="'${description['section-heading']}'" 
                           v-bind:default-text="'write text here'"
                        >
                        </text-editor>`
                        : `<h1 class="section-heading"> ${description['section-heading']} </h1>`
                    }
                    
                   ${editable ?
                        `<text-editor 
                            v-bind:position="['description', 'desc']" 
                            v-bind:tag="'p'"
                            v-bind:value-text="'${description['desc']}'" 
                            v-bind:default-text="'write text here'"
                          ></text-editor>`
                        : `<p>${description['desc']}</p>`
                    }
                  </div>
               </div>
             
            </div>
         </div>
        <div class="container">
            <div class="row mt-2">
              <div class="ts-map" style="width: 100%;">
                 <div class="mapouter">
                    <div class="gmap_canvas" style="width: 100%;border-radius: 0;height: 200px;">
                        <div style="position: relative;display: flex;align-items: center;justify-content: center;">
                            <img src="/assets/map-temp.png" alt="map-temp" />
                          <a class="map-btn" href="${data?.location_url}" target="_blank">
                                <i class="fa fa-map"></i>
                                ${words['Open Event Location']}
                            </a>
                        </div>
                    </div>
                 </div>
              </div>
           </div>
        </div>
      </section>    
`
}

const footer = (editable, isRequired, data = {}, content, words, lang, link, logo, footerText, footerHeading) => {
    return `
      <footer v-if="sections.some((i) => i === 'footer') || ${isRequired}" class="footer pt-3 pb-4 mx-0" style="background: url(\/event-temp/temp-1/images/bg/footer_bg.png\);">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                    <div style="flex-direction: row;display: flex;justify-content: center; margin-bottom: 8px;align-items: center;">
                         <a target="_blank" href="${editable ? 'javascript:void(0)' :  link }" style="padding: 0 10px; display: block;">
                            <img style="width: 68px; margin: auto; display: block;filter: brightness(2.5);" src="${logo}" alt="logo" />
                        </a>     
                        <p style="text-align: ${lang === 'en' ? 'left' : 'right'};color: #efefef;margin: 4px 7px; max-width: 400px;">${footerText}</p>
                    </div>
                  <div class="copyright-text text-center">
                     <p>© ${new Date().getFullYear()}, <a href="${link}">Eventiqr</a> ${getDefaultText(lang, 'All rights reserved')}.</p>
                  </div>
               </div>
            </div> 
         </div>
      </footer>
    `
}

const comments = (editable, isRequired, data = {}, {comments}) => {
    return `
        <section v-if="sections.some((i) => i === 'comments') || ${isRequired}" class="business-conference" style="padding-top: 90px; background: url(\/event-temp/temp-1/images/bg/feature_bg.png\);">
             <div class="container">
                <div class="row">
                   <div class="col-md-10 mx-auto">
                      <div class="section-title-area pb-0">
                      
                      ${editable ?
                            `<text-editor 
                                v-bind:position="['comments', 'sub-title']" 
                                v-bind:tag="'span'" 
                                v-bind:class-name="'sub-title'" 
                                v-bind:value-text="'${comments['sub-title']}'" 
                                v-bind:default-text="'Leave a Comment'"></text-editor>`
                            : `<span class="sub-title">${comments['sub-title'] || `Comments`}</span>`
                     }
                      
                      ${editable ?
                            `<text-editor 
                                v-bind:position="['comments', 'section-heading']" 
                                v-bind:tag="'h2'" 
                                v-bind:class-name="'section-heading pb-0'" 
                                v-bind:value-text="'${comments['section-heading']}'" 
                                v-bind:default-text="'Comments'"></text-editor>`
                            : `<h2 class="section-heading pb-0">${comments['section-heading'] || `Comments`}</h2>`
                      }
                      </div>
                   </div>
                </div>
                <div class="row">
                    <comments v-bind:id="${data.id}" />
                </div>
            </div>
        </section>
    `
}

const speakers = (editable, isRequired, data = {}, {speakers}, words, lang) => {
    return `
       <section v-if="sections.some((i) => i === 'speakers') || ${isRequired}" id="speakers-section" class="speakers" style="background: url(\/event-temp/temp-1/images/bg/team_bg.png\);">
         <div class="container">
            <div class="row">
               <div class="col-md-8 text-center mx-auto">
                  <div class="section-title-area">
                          
                     ${editable ?
                        `<text-editor 
                            v-bind:position="['speakers', 'sub-title']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'sub-title'" 
                            v-bind:value-text="'${speakers['sub-title']}'" 
                            v-bind:default-text="'Sub Title'"></text-editor>`
                        : `<span class="sub-title">${speakers['sub-title']}</span>`
                    }
                     
                     ${editable ?
                        `<text-editor 
                            v-bind:position="['speakers', 'title']" 
                            v-bind:tag="'h2'" 
                            v-bind:class-name="'section-heading'" 
                             v-bind:value-text="'${speakers['title']}'" 
                            v-bind:default-text="'Speakers'"></text-editor>`
                        : `<h2 class="section-heading">${speakers['title']}</h2>`
                    }
                     
                  </div>
               </div>
                      
            </div>
                <loop-item 
                    v-bind:editable="${editable}"
                    v-bind:default-list='${speakers['speakers'] ? JSON.stringify(speakers['speakers']) : '{}' }'
                    v-bind:position="['speakers', 'speakers']" 
                    v-bind:parent-id="'speakers-section'"
                    v-bind:class-name="'row justify-content-center'"
                    v-bind:inputs="[{label: '${getDefaultText(lang, 'Name')}', name: 'name', type: 'text'}, {label: '${getDefaultText(lang, 'Title')}',name: 'title', type: 'text'}, {label: '${getDefaultText(lang, 'Avatar')}',name: 'img', type: 'img'}]"
                    v-slot="slotProps" 
                    >
                    <div class="col-lg-4 col-md-6">
                        <div class="ts-speaker overlay-1">
                             <div class="speaker-img">
                                <img class="img-fluid" :src="slotProps.content.img || 'https://via.placeholder.com/400x400'" alt="">
                                <div class="content">
                                   <h3 class="ts-title">{{slotProps.content.name || 'Name'}}</h3>
                                   <p> {{slotProps.content.title || 'Title'}} </p>
                                </div>
                             </div>
                        </div>
                    </div>
                </loop-item>
         </div>
      </section>
`
}

const sponsors = (editable, isRequired, data = {}, {sponsors}) => {
    return `
     <section v-if="sections.some((i) => i === 'sponsors') || ${isRequired}" class="ts-sponsors" id="sponsors-section">
         <div class="container">
            <div class="row">
               <div class="col-lg-5">
                  <div class="section-title-area">
                  
                    ${editable ?
                        `<text-editor 
                            v-bind:position="['sponsors', 'sub-title']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'sub-title'" 
                            v-bind:value-text="'${sponsors['sub-title']}'" 
                            v-bind:default-text="'Sub Title'"></text-editor>`
                        : `<span class="sub-title">${sponsors['sub-title']}</span>`
                    }
                
                     ${editable ?
                        `<text-editor 
                            v-bind:position="['sponsors', 'title']" 
                            v-bind:tag="'h2'" 
                            v-bind:class-name="'section-heading'" 
                            v-bind:value-text="'${sponsors['title']}'" 
                            v-bind:default-text="'Sponsors'"></text-editor>`
                        : `<h2 class="section-heading">${sponsors['title']}</h2>`
                    }                                   
                   
                   ${editable ?
                        `<text-editor 
                            v-bind:position="['sponsors', 'text']" 
                            v-bind:tag="'p'" 
                            v-bind:class-name="''" 
                            v-bind:value-text="'${sponsors['text']}'" 
                            v-bind:default-text="'Sponsors Description'"></text-editor>`
                        : `<p>${sponsors['text']}</p>`
                    }    
                   
                  </div>
               </div>
               <div class="col-lg-7">
                  <div class="sponsors-wrap">
                   <images-uploader
                        v-bind:editable="${editable}"
                        v-bind:default-list="'${sponsors['logos'] || []}'"
                        v-bind:parent-id="'sponsors-section'" 
                        v-bind:position="['sponsors', 'logos']"
                        v-bind:min="3" 
                        v-bind:max="6" 
                        v-bind:class-name="'row'" 
                        v-slot="slotProps" >
                       <div class="col-md-4">
                           ${editable ?
                                `<a-button @click="slotProps.deleteItem(slotProps.index)" class="images-uploader-delete-btn">
                                  <a-icon type="delete" />
                                </a-button>` : ''
                            }
                           <span class="sponsors-logo">
                              <img class="img-fluid" :src="slotProps.content || 'https://via.placeholder.com/135x36'" alt=""/>
                           </span>
                       </div>
                   </images-uploader>
                  </div>
               </div>
            </div>
         </div>
      </section>
    `
}

const gallery = (editable, isRequired, data = {}, {gallery}) => {
    return `
      <section v-if="sections.some((i) => i === 'gallery') || ${isRequired}" class="ts-venue section-bg" id="gallery-section">
        <div class="container">
				<div class="row">
						<div class="col-lg-12">
						${editable ?
                            `<text-editor 
                                v-bind:position="['gallery', 'title']" 
                                v-bind:tag="'h2'" 
                                v-bind:class-name="'section-title'" 
                                v-bind:value-text="'${gallery['title']}'" 
                                v-bind:default-text="'Gallery'"></text-editor>`
                            : `<h2 class="section-title">${gallery['title']}</h2>`
                        }
						</div>
					</div>
            
            <images-uploader
                v-bind:editable="${editable}"
                v-bind:default-list="'${gallery['images'] || []}'"
                v-bind:parent-id="'gallery-section'" 
                v-bind:position="['gallery', 'images']"
                v-bind:min="3" 
                v-bind:max="6" 
                v-bind:class-name="'row'" 
                v-slot="slotProps" >
               <div class="col-lg-3 col-md-6 mb-3">
                   ${editable ?
                    `<a-button @click="slotProps.deleteItem(slotProps.index)" class="images-uploader-delete-btn">
                      <a-icon type="delete" />
                    </a-button>` : ''
                    }
                   <div class="venue-img">
                     <a href="javascript:void(0)" class="ts-popup">
                        <img style="width: 100%;" :src="slotProps.content || 'https://via.placeholder.com/135x36'" :alt="slotProps.content || 'https://via.placeholder.com/135x36'" />
                     </a>
                  </div>
               </div>
           </images-uploader>
                  
         </div>
      </section>
    `
}

const banner = (editable, isRequired, data = {}, {banner}) => {
    return `
    <section id="banner-section-bg" v-if="sections.some((i) => i === 'banner') || ${isRequired}" class="ts-intro-sponsors" 
        style="background-image: url(${banner['banner-bg'] ? banner['banner-bg'] : ''});"
      >
      
        ${editable ? `<image-panel
            v-bind:position="['banner', 'banner-bg']"
            v-bind:is-bg="true"
            v-bind:element-id="'banner-section-bg'"
            v-bind:url="'${banner['banner-bg'] ? banner['banner-bg'] : ''}'"
            ></image-panel>
            `: ''
        }
        
        <div class="container">
            <div class="row">
               <div class="col-md-10 text-center mx-auto">
                  <div class="section-title-area">
                  
                  ${editable ?
                    `<text-editor 
                        v-bind:position="['banner', 'top-title']" 
                        v-bind:tag="'span'" 
                        v-bind:class-name="'sub-title'" 
                        v-bind:value-text="'${banner['top-title']}'" 
                        v-bind:default-text="'Sub Title'"
                        ></text-editor>`
                    : `<span class="sub-title">${banner['top-title']}</span>`
                }
                  
                                    
                  ${editable ?
                    `<text-editor 
                        v-bind:position="['banner', 'title']" 
                        v-bind:tag="'h2'" 
                        v-bind:class-name="'section-heading'" 
                        v-bind:value-text="'${banner['title']}'" 
                        v-bind:default-text="'Banner Title'"></text-editor>`
                    : `<span class="section-heading">${banner['title']}</span>`
                    }
                  </div>
               </div>
            </div>
         </div>
    </section>
    `
}

const counter = (editable, isRequired, data = {}, {counter}) => {
    return `

        <section v-if="sections.some((i) => i === 'counter') || ${isRequired}" class="pt-0">
            <div class="row pt-100">
               <div class="col-lg-3 col-md-6">
                     <div class="ts-single-funfact green-br">
                     <h3 class="funfact-num">
                     ${editable ?
                            `<number-editor
                                v-bind:position="['counter', '0-number']" 
                                v-bind:tag="'span'" 
                                v-bind:class-name="'counterUp'" 
                                v-bind:default-text="'${counter['0-number'] || '300'}'"></number-editor>`
                            : `<span class="counterUp" data-counter="${counter['0-number'] || '300'}">${counter['0-number'] || '300'}</span>`
                        }
                     +</h3>
                     ${editable ?
                        `<text-editor 
                            v-bind:position="['counter', '0-text']" 
                            v-bind:tag="'h4'" 
                            v-bind:class-name="'funfact-title'" 
                            v-bind:value-text="'${counter['0-text']}'" 
                                v-bind:default-text="'Text'"></text-editor>`
                        : `<h4 class="funfact-title">${counter['0-text']}</h4>`
                    }
                  </div>
               </div>
               <div class="col-lg-3 col-md-6">
                     <div class="ts-single-funfact yello-br">
                     <h3 class="funfact-num">
                     ${editable ?
                        `<number-editor 
                            v-bind:position="['counter', '1-number']" 
                            v-bind:tag="'span'" 
                            v-bind:class-name="'counterUp'" 
                            v-bind:default-text="'${counter['1-number'] || '300'}'"></number-editor>`
                        : `<span class="counterUp" data-counter="${counter['1-number'] || '300'}">${counter['1-number'] || '300'}</span>`
                    }
                     +</h3>
                     ${editable ?
                            `<text-editor 
                                v-bind:position="['counter', '1-text']" 
                                v-bind:tag="'h4'" 
                                v-bind:class-name="'funfact-title'" 
                                 v-bind:value-text="'${counter['1-text']}'" 
                        v-bind:default-text="'Text'"></text-editor>`
                            : `<h4 class="funfact-title">${counter['1-text']}</h4>`
                        }
                  </div>
               </div>              
               <div class="col-lg-3 col-md-6">
                 <div class="ts-single-funfact red-br">
                     <h3 class="funfact-num">
                     ${editable ?
                            `<number-editor
                                v-bind:position="['counter', '2-number']" 
                                v-bind:tag="'span'" 
                                v-bind:class-name="'counterUp'" 
                                v-bind:default-text="'${counter['2-number'] || '300'}'"></number-editor>`
                            : `<span class="counterUp" data-counter="${counter['2-number'] || '300'}">${counter['2-number'] || '300'}</span>`
                        }
                     +</h3>
                     ${editable ?
                            `<text-editor 
                                v-bind:position="['counter', '2-text']" 
                                v-bind:tag="'h4'" 
                                v-bind:class-name="'funfact-title'" 
                               v-bind:value-text="'${counter['2-text']}'" 
                        v-bind:default-text="'Text'"></text-editor>`
                            : `<h4 class="funfact-title">${counter['2-text']}</h4>`
                        }
                  </div>
               </div> 
               <div class="col-lg-3 col-md-6">
                     <div class="ts-single-funfact violet-br">
                     <h3 class="funfact-num">
                         ${editable ?
                            `<number-editor
                                v-bind:position="['counter', '3-number']" 
                                v-bind:tag="'span'" 
                                v-bind:class-name="'counterUp'" 
                                 v-bind:default-text="'${counter['3-number'] || '300'}'"></number-editor>`
                                : `<span class="counterUp" data-counter="${counter['3-number'] || '300'}">${counter['3-number'] || '300'}</span>`
                        }
                     +</h3>
                     ${editable ?
                        `<text-editor 
                            v-bind:position="['counter', '3-text']" 
                            v-bind:tag="'h4'" 
                            v-bind:class-name="'funfact-title'" 
                            v-bind:value-text="'${counter['3-text']}'" 
                            v-bind:default-text="'Text'"></text-editor>`
                        : `<h4 class="funfact-title">${counter['3-text']}</h4>`
                    }
                  </div>
               </div>
            </div>   
        </section>
`
}

const countdown = (editable, isRequired, data = {countdown}) => {
    return ''
    return `
        <div v-if="sections.some((i) => i === 'countdown') || ${isRequired}">countdown</div>
    `
}

const video = (editable, isRequired, data = {}, {video}) => {
    return `
      <section v-if="sections.some((i) => i === 'video') || ${isRequired}" class="main-container pb-0" id="main-container">
       <div class="container">
        <div class="row">
         <div class="col-lg-10 mx-auto">
          <div class="blog-details">
           <div class="entry-header">
           ${editable ?
                `<text-editor 
                    v-bind:position="['video', 'title']" 
                    v-bind:tag="'h2'" 
                    v-bind:class-name="'entry-title text-center'" 
                    v-bind:value-text="'${video['title']}'" 
                    v-bind:default-text="'Video'"></text-editor>`
                : `<h2 class="entry-title text-center">${video['title']}</h2>`
            }
           </div>
           <div class="post-video post-media post-image text-center mt-5 mb-5" style="max-width: 100%;width: 100%;min-height: 530px;">
           
           ${video?.iframe ?
                `<iframe 
                    style="max-width: 100%;width: 100%;min-height: 530px;" 
                    src="${video?.iframe}" 
                    title="YouTube video player" 
                    frameborder="0" 
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                    allowfullscreen
                >
                </iframe>`
                : `<img class="img-fluid" src="https://via.placeholder.com/800x500?text=video" alt="">`
            }
         
            ${editable ?
                `<video-input v-bind:position="['video', 'iframe']" v-bind:value="'${video?.iframe}'"  />`
                : ``
            }
                
           </div>
          </div>
         </div>
        </div>
       </div>
      </section>
    `
}

const temp = (editable, {requiredSection, color, data = {}, content, language, words, invitation, link, logo, footerText, footerHeading, csrf}) => {

    return  `
    <html data-token="${csrf}" lang="${language?.code || 'en'}" dir="${language?.direction || 'ltr'}" data-category-id="${data?.category_id}" data-theme-color="${color || '#13dc38'}" data-event-id="${data.id}" data-invitation-code="${invitation?.data?.invitation?.code}">
        <head>
        
           <meta charset="utf-8">
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
           <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
           <title>Eventiqr - ${data?.name}</title>
           
           <link rel="stylesheet" href="/event-temp/temp-1/css/bootstrap.min.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/font-awesome.min.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/animate.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/magnific-popup.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/owl.carousel.min.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/isotop.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/xsIcon.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/style.css">
           <link rel="stylesheet" href="/event-temp/temp-1/css/responsive.css">
        
        ${editable ? `
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.22.1/css/medium-editor.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/medium-editor/5.22.1/css/themes/flat.css">
        ` : ''}
    
           <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
           <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
           <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        
        <style>
        .section-title-area .sub-title p {
            font-size: inherit;
        }
        .ts-intro-sponsors .section-title-area .sub-title {
            display: block;
        }
        .ts-intro-sponsors .section-title-area .sub-title{
            color: #ffffff;
         }
         .hero-content-wrapper .button-and-social-area {
            overflow: visible;
         }
         
        .download-ticket-btn{
            margin: 0 5px !important;
        }
       .sub-title, .sub-title p{
            color: #b9b9b9  !important;
         }


        .copyright-text a, a.copyright-text{
            color: #ffffff !important;
        }
        
        .section-title-area .sub-title {
            font-size: 27px;
            letter-spacing: 1px;
        }
        
        .map-btn {
          background: #55d934;
          background-image: -webkit-linear-gradient(top, #55d934, #55d934);
          background-image: -moz-linear-gradient(top, #55d934, #55d934);
          background-image: -ms-linear-gradient(top, #55d934, #55d934);
          background-image: -o-linear-gradient(top, #55d934, #55d934);
          background-image: linear-gradient(to bottom, #55d934, #55d934);
          border-radius: 28px;
          color: #ffffff  !important;
          font-size: 20px;
          padding: 10px 20px 10px 20px;
          text-decoration: none;
          position: absolute;
        }
        
        .map-btn:hover {
          background: #55d934;
          background-image: -webkit-linear-gradient(top, #55d934, #55d934);
          background-image: -moz-linear-gradient(top, #55d934, #55d934);
          background-image: -ms-linear-gradient(top, #55d934, #55d934);
          background-image: -o-linear-gradient(top, #55d934, #55d934);
          background-image: linear-gradient(to bottom, #55d934, #55d934);
          color: #ffffff !important;
        }
                        
                            ${`
                .banner-btn .btn.download-ticket-btn{
                    color: ${color || '#13dc38'} !important;
                }
                .banner-btn .btn.download-ticket-btn i{
                    color: ${color || '#13dc38'} !important;
                }
                .banner-btn .btn.attended-dropdown:hover{
                    background: ${color || '#13dc38'} !important;
                    border-color: ${color || '#13dc38'} !important;
                }
                .btn{
                    background: ${color || '#13dc38'} !important;
                    border-color: ${color || '#13dc38'} !important;
                }
                .hero-content-wrapper .button-and-social-area .register-btn {
                    border: 2px solid #ffffff  !important;
                }

            `}
                            
            
         ${
            color ? `
                .hero-content-wrapper{
                    background-color: ${color}d4 !important;
                }
                .ts-speaker .speaker-img .content .ts-title, .BackTo, .ts-intro-sponsors::before, .btn{
                    background-color: ${color}  !important;
                }
            
            ` : `
                .ts-intro-sponsors .section-title-area .sub-title {
                    color: #25cd44;
                }
            `
            }
    
        </style>
        
        </head>
        
        <body>
           <div  id="eventiqr-panel">


            ${content.basic ? basic(editable, requiredSection.some((i) => i === 'basic'), data, content, words, language?.code, invitation) : ''}
            ${content.description ? description(editable, requiredSection.some((i) => i === 'description'), data , content, words, language?.code) : ''}
            ${content.counter ? counter(editable, requiredSection.some((i) => i === 'counter'), data, content, words, language?.code) : ''}
            ${content.countdown ? countdown(editable, requiredSection.some((i) => i === 'countdown'), data, content, words, language?.code) : ''}
            ${content.speakers ? speakers(editable, requiredSection.some((i) => i === 'speakers'), data, content, words, language?.code) : ''}
            ${content.video ? video(editable, requiredSection.some((i) => i === 'video'), data, content, words, language?.code) : ''}
            ${content.gallery ? gallery(editable, requiredSection.some((i) => i === 'gallery'), data, content, words, language?.code) : ''}
            ${content.sponsors ? sponsors(editable, requiredSection.some((i) => i === 'sponsors'), data, content, words, language?.code) : ''}
            ${content.comments ? comments(editable, requiredSection.some((i) => i === 'comments'), data, content, words, language?.code) : ''}
            ${content.banner ? banner(editable, requiredSection.some((i) => i === 'banner'), data, content, words, language?.code) : ''}
            ${footer(editable, requiredSection.some((i) => i === 'footer'), data, content, words, language?.code, link, logo, footerText, footerHeading)}

              <div class="BackTo">
                 <a href="#" class="fa fa-angle-up" aria-hidden="true"></a>
              </div>

           </div>


                <script>
                    window.words = ${JSON.stringify(words)}
                </script>
            
              <script src="/event-temp/temp-1/js/jquery.js"></script>
        
              <script src="/event-temp/temp-1/js/popper.min.js"></script>

              <script src="/event-temp/temp-1/js/bootstrap.min.js"></script>

              <script src="/event-temp/temp-1/js/jquery.appear.min.js"></script>

              <script src="/event-temp/temp-1/js/jquery.jCounter.js"></script>

              <script src="/event-temp/temp-1/js/jquery.magnific-popup.min.js"></script>

              <script src="/event-temp/temp-1/js/owl.carousel.min.js"></script>
    
              <script src="/event-temp/temp-1/js/wow.min.js"></script>
  
              <script src="/event-temp/temp-1/js/isotope.pkgd.min.js"></script>

              <script src="/event-temp/temp-1/js/main.js"></script>

              <script src="/assets/frontend/panel/js/manifest.min.js"></script>
              <script src="/assets/frontend/panel/js/vendor.min.js"></script>
              <script src="/assets/frontend/panel/js/script.min.js"></script>
              <script src="/assets/frontend/panel/js/app.min.js"></script>


  

        </body>

    </html>
  `
}

export default temp
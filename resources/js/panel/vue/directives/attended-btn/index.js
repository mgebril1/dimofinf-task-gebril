import Vue from "vue"

const STATUS = {
  going: 'Going',
  maybe: 'Maybe',
  not_going: 'Not Going',
  attended: 'Attended',
}

Vue.directive('attended-btn', {
  update: function (target, binding) {

    let ATTEND_VAL = 'attended'
    let ATTEND_TEXT = STATUS['attended']

    const checkAttend = () => {
      const token = sessionStorage.getItem('token');
      const eventId = document.getElementsByTagName('html')[0].getAttribute('data-event-id')

      const data = {
        event_id: eventId,
      }

      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }

      axios.post('/api/event/check-attend', data, config).then((res) => {
        ATTEND_VAL = res.data.Response.status;
        ATTEND_TEXT = STATUS[res.data.Response.status];
        target.childNodes[0].textContent = ATTEND_TEXT;
      }).catch((e) => {

      })
    }

    checkAttend()

    const changeAttend = (val) => {
      const token = sessionStorage.getItem('token');
      const eventId = document.getElementsByTagName('html')[0].getAttribute('data-event-id')
      const oldAttendText = target.childNodes[0].textContent

      if(!token){
        window.location.href = `/${eventId}/login`
        return;
      }

      if(val === binding.value){
        return
      }

      target.childNodes[0].textContent = 'Loading...'

      const data = {
        event_id: eventId,
        status: val
      }

      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      }

      axios.post('/api/event/attend', data, config).then((res) => {
        if(res.data.Error.status){
          ATTEND_VAL = res.data.Response.status;
          ATTEND_TEXT = STATUS[res.data.Response.status];
          target.childNodes[0].textContent = STATUS[res.data.Response.status]
        }else {
          target.childNodes[0].textContent = oldAttendText;
        }
      }).catch((err) => {
        if(err.response.status === '401'){
          window.location.href = `/${eventId}/login`
        }
        target.childNodes[0].textContent = oldAttendText;
      })
    }

    target.classList?.add('attended-dropdown');

    const ul = document.createElement('ul');
    ul.classList?.add('attended-dropdown-content');

    [{
      name: STATUS.going,
      value: 'going'
    },{
      name: STATUS.maybe,
      value: 'maybe'
    },{
      name: STATUS.not_going,
      value: 'not_going'
    }].map(({name, value}) => {
      const li = document.createElement('li');
      li.onclick = function () {
        changeAttend(value)
      };

      li.innerText = name;
      ul.appendChild(li)
    })

    target.appendChild(ul)

  },
  inserted: function (target, binding) {
    const style = document.createElement('style');
    const themeColor = document.getElementsByTagName('html')[0].getAttribute('data-theme-color')
    const targetStyle = window.getComputedStyle(target);

    style.append(`
          .attended-dropdown{
            position: relative;
            display: flex;
            justify-content: center;
          }
          
          .attended-dropdown .attended-dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            padding: 12px 16px;
            z-index: 1;
            
            top: ${target.offsetHeight - targetStyle['border'].split('px')[0]}px;
          }
          
          .attended-dropdown .attended-dropdown-content {
            color: ${themeColor};
          }
          
          .attended-dropdown:hover .attended-dropdown-content {
            display: block;
          }
      `)
    document.body.append(style)
  },
});
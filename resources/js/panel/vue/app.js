require('./bootstrap')
import Vue from "vue"
import editor from 'vue2-medium-editor';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { BIcon } from 'bootstrap-vue';
import { Upload, Button, Icon, Modal } from 'ant-design-vue';
import { GooglePlacesAutocomplete } from 'vue-better-google-places-autocomplete'
import Fragment from 'vue-fragment'
import html2canvas from 'html2canvas';
const TICKET_1 = require('./components/TICKETS/TICKET_1').default
import contenteditable from 'vue-contenteditable'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'ant-design-vue/lib/upload/style/index.css'
import 'ant-design-vue/lib/button/style/index.css'
import 'ant-design-vue/lib/icon/style/index.css'
import 'ant-design-vue/lib/modal/style/index.css'


require('./directives/attended-btn/index')


window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.Vue = Vue;
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Upload);
Vue.use(Button);
Vue.use(Icon);
Vue.use(Modal);
Vue.use(Fragment.Plugin)
Vue.use(contenteditable)
// components
Vue.component('BIcon', BIcon)
Vue.component('editor', editor);
Vue.component('google-places-autocomplete', GooglePlacesAutocomplete)
Vue.component('img-lazy', require('./components/img-lazy').default);
Vue.component('preview-event', require('./components/preview-event').default);
Vue.component('text-editor', require('./components/editable-content-components/text-editor').default);
Vue.component('number-editor', require('./components/editable-content-components/number-editor').default);
Vue.component('date-time', require('./components/editable-content-components/date-time').default);
Vue.component('loop-item', require('./components/editable-content-components/loop-item').default);
Vue.component('comments', require('./components/editable-content-components/comments').default);
Vue.component('images-uploader', require('./components/editable-content-components/images-uploader').default);
Vue.component('address-text', require('./components/editable-content-components/address-text').default);
Vue.component('image-panel', require('./components/editable-content-components/image-panel').default);
Vue.component('video-input', require('./components/editable-content-components/video-input').default);
Vue.component('event-editable-view', require('./components/event-editable-view').default);
Vue.component('basic-information-form', require('./components/basic-information-form').default);
Vue.component('address-input', require('./components/address-input').default);
Vue.component('theem-select', require('./components/theem-select').default);
Vue.component('auth-form', require('./components/auth-form').default);
Vue.component('AttendedBtn', require('./components/attended-btn').default);
Vue.component('sections-select', require('./components/sections-select').default);


const EventiqrPanel = new Vue({
    el: '#eventiqr-panel',
    data: function () {
        return {
            sections: [],
            content: {},
            basicInformationForm: {},
            eventData: {},
            invitation: {},
            theme: null
        }
    },
    watch: {
        eventData(val) {
            if(val?.id){
                const extraSections = val.extra_sections ? JSON.parse(val.extra_sections) : {}
                this.content = extraSections?.content || {};
                this.sections = extraSections?.sections || [];
            }
            if(val?.id && val?.theme){
                this.theme = val.theme;
            }
        },
        content(val) {
            // console.log("content", val)
        },
        sections(val) {
            // console.log("sections", val)
        },
        theme(val) {
        },
    },
    methods: {
        changeContent(message) {
            this.content = {
                ...this.content,
                [message.data.section]:
                    Object.assign(
                        this.content[message?.data?.section] ?
                            this.content[message?.data?.section] : {},
                        {[message.data.name]: message.data.value}
                    )
            }
        },
        save(cb) {
            const {duration, end_date, start_date, user_id} = this.eventData;
            const extraSections = {
                sections: this.sections,
                content: this.content,
            }
            axios.post(`/api/update_event/${this.eventData.id}`,
                {
                    ...this.basicInformationForm,
                    duration,
                    end_date,
                    start_date,
                    user_id,
                    extra_sections: JSON.stringify(extraSections),
                    theme: this.theme.id
                }
            )
                .then((res) => {
                    cb({
                        ...this.eventData,
                        ...res.data.Response
                    })
                    this.eventData = {
                        ...this.eventData,
                        ...res.data.Response
                    }
                })
                .catch((e) => cb(e))
        },
        downloadTicket() {

            console.log("this.invitation -> app.js -> downloadTicket", this.invitation)

            if(this.invitation?.data){

                if(this.invitation?.data?.id){

                    console.log("this.invitation -> app.js", this.invitation?.data)

                    const a = document.createElement('a')
                    a.setAttribute('download', `${this.invitation?.data?.event?.name} - ticket.pdf`)
                    a.setAttribute('href', `/api/download-invitation/${this.invitation?.data?.id}`);
                    document.body.appendChild(a);
                    a.click();
                    return;
                }

                const html = TICKET_1(this.invitation);
                const iframe = document.createElement("iframe");
                iframe.id = 'ticket-frame'
                document.body.appendChild(iframe);
                iframe.contentWindow.document.open();
                iframe.contentWindow.document.write(html);
                iframe.contentWindow.document.close();
                $('#ticket-frame').on('load', () => {
                    setTimeout(() => {
                        html2canvas(iframe.contentWindow.document.body).then(canvas => {
                            const dataURL = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
                            const a = document.createElement('a')
                            a.setAttribute('download', `${this.invitation?.data?.event?.name} - ticket.png`)
                            a.setAttribute('href', dataURL)
                            a.click()
                            canvas.remove();
                            const element = document.getElementById("ticket-frame");
                            element.parentNode.removeChild(element);
                        });
                    }, 1000)
                });
            }
        }
    },
    mounted() {
        console.log('%c Starting Eventiqr Panel App!', 'background: #19823c; color: #ffffff');
        const _this = this;

        this.$nextTick(() => {
            const bc_sectionsChange = new BroadcastChannel('sections:change');
            bc_sectionsChange.onmessage = function(message){
                _this.sections = message.data.sections;
            };
        })

        this.$nextTick(() => {
            const bc_contentChange = new BroadcastChannel('content:change');
            bc_contentChange.onmessage = function(message){
                _this.changeContent(message)
            };
        })

        this.$nextTick(() => {
            const bc_downloadTicket = new BroadcastChannel('downloadTicket');
            bc_downloadTicket.onmessage = function(message){
                _this.downloadTicket()
            };
        })
    }
});

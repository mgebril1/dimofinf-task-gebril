require('./bootstrap')
import Vue from "vue"

window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.Vue = Vue;

// components
Vue.component('img-lazy', require('./components/img-lazy').default);

const eventiqr = new Vue({
    el: '#eventiqr',
    mounted() {
        console.log('%c Starting Eventiqr App!', 'background: #19823c; color: #ffffff');
    }
});

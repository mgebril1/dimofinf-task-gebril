import camelToSnake from './camel-to-snake';

export default function (obj){
    let object = {};
    Object.keys(obj).map((key) => object[camelToSnake(key)] = obj[key]);
    return object;
}

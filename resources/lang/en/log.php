<?php

return [
    'create_admin' => 'created a new admin',
    'update_admin' => 'update data of ',
    'delete_admin' => 'deleted admin ',

    'create_role' => 'created a new role',
    'update_role' => 'update data of ',
    'delete_role' => 'deleted role ',

    'create_student' => 'created a new student',
    'update_student' => 'update data of ',
    'delete_student' => 'deleted student ',

    'create_school' => 'created a new school',
    'update_school' => 'update data of ',
    'delete_school' => 'deleted school ',

    'create_teacher' => 'created a new teacher',
    'update_teacher' => 'update data of ',
    'delete_teacher' => 'deleted teacher ',

    'create_parent' => 'created a new parent',
    'update_parent' => 'update data of ',
    'delete_parent' => 'deleted parent',

    'create_education' => 'created a new education type',
    'update_education' => 'update data of ',
    'delete_education' => 'deleted education type',
];
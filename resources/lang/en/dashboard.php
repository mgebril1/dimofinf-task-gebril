<?php

return [
    'dashboard' => 'Dashboard',
    'management' => 'Management',
    'content' => 'Contents',
    'profile' => 'My profile',
    'lang' => 'عربي',
    'log' => 'LOG',
    'old_password' => 'Current password',

    'change_password' => 'Change Password',
    'logout' => 'Logout',
    'topic_title' => 'Topic Title',


    'save' => 'Save',
    'reset' => 'Reset',
    'name' => 'Name',
    'control' => 'Control',

    'roles' => 'Roles',
    'roles_list' => 'Roles',
    'create_role' => 'Add Role',
    'edit_role' => 'Edit Role',
    'role_name' => 'Role Name',
    'permissions' => 'Permissions',

    'countries' => 'Countries',
    'countries_list' => 'Countries',
    'create_country' => 'Add Country',
    'edit_country' => 'Edit Country',

    'governments' => 'Governments',
    'governments_list' => 'Governments',
    'create_government' => 'Add Government',
    'edit_government' => 'Edit Government',

    'cities' => 'Cities',
    'cities_list' => 'Cities',
    'create_city' => 'Add City',
    'edit_city' => 'Edit City',

    'users' => 'Users',
    'users_list' => 'Users',
    'create_user' => 'Add User',
    'edit_user' => 'Edit User',
    'user_address' => 'User Address',

    'admins' => 'Admins',
    'admins_list' => 'Admins List',
    'create_admin' => 'Add Admin',
    'edit_admin' => 'Edit Admin',

    'features' => 'Features',
    'features_list' => 'Features List',
    'create_feature' => 'Add Feature',
    'edit_feature' => 'Edit Feature',


    'brands' => 'Brands',
    'brands_list' => 'Brands List',
    'create_brand' => 'Add Brand',
    'edit_brand' => 'Edit Brand',

    'models' => 'Models',
    'models_list' => 'Models List',
    'create_model' => 'Add Model',
    'edit_model' => 'Edit Model',

    'options' => 'Options',
    'options_list' => 'Options List',
    'create_option' => 'Add Option',
    'edit_option' => 'Edit Option',

    'languages' => 'Languages',
    'languages_list' => 'Languages List',
    'create_language' => 'Add Language',
    'edit_language' => 'Edit Language',
    'code' => 'Code',
    'dir' => 'Direction',
    'web_settings' => 'Website settings',
    'web_title' => 'Website Title',
    'address' => 'Our address',
    'seo' => 'SEO',
    'word' => 'Word',
    'translation' => 'Translation',
    'dictionary' => 'Dictionary',


    'edit' => 'Edit',
    'data' => "'s data",



    'email' => 'E-mail',
    'admin_role' => 'Role',
    'status' => 'Status',

    'is_active' => 'Is active?',
    'admin_name' => 'User Name',
    'admin_email' => 'User E-mail',
    'admin_image' => 'Image',
    'admin_mobile' => 'Mobile',
    'password' => 'Password',
    'password_confirm' => 'Password Confirmation',
    'meta_desc' => 'Meta description',
    'meta_title' => 'Meta title',
    'meta_keywords' => 'Meta Keyword',


    'close' => 'Close',
    'add' => 'Add',
    'delete' => 'Delete',


    'pages_list' => 'Pages List',
    'pages' => 'All pages',
    'title' => 'Title',
    'create_page' => 'Create page',
    'language' => 'Language',
     'parent_page' => 'Parent page',
    'content_page' => 'Page Content',
    'banner' => 'Banner',
    'is_header' => 'Is header page?',
    'is_footer' => 'Is footer page ?',
    'preview' => 'Preview',
    'page_data' => 'Page data',

    'is_services' => 'Show Services ?',
    'is_teams' => 'Show Team ?',
    'is_testmonials' => 'Show testimonial?',
    'is_blogs' => 'Show recent blogs ?',
    'is_blog' => 'Is Blogs page ?',
    'is_counters' => 'Show Counter ?',
    'is_partners' => 'Show partners ?',
    'is_subscribe' => 'Show subscribe form ?',
    'is_about' => 'Show about ? ',
    'is_eduction' => 'IS Education types page ?',
    'page_order' => 'Page Order',

     'preview_image' => 'Preview Image',
    'page_banner' => 'Page banner',
    'duration' => ' Duration',



    'sliders' => 'Slider',
    'sliders_list' => 'Slides List',
    'create_slider' => 'Create Slide',
    'update_slider' => 'Update slider',
    'active' => 'Active',
    'disactive' => 'Inactive',
    'desc' => 'Description',
    'btn' => 'Button Label',
    'image' => 'Image',
    'link' => 'Button Target',
    'slider_date' => "Image's details",

    'services' => 'Service',
    'services_list' => 'Services List',
    'create_service' => 'Create Service',
    'update_service' => 'Update service',
    'service_date' => "Image's details",
    'icon' => "Icon",

    'testmonials' => 'Testimonial',
    'testmonials_list' => 'Testimonial List',
    'create_testmonial' => 'Create Testimonial',
    'update_testmonial' => 'Update Testimonial',
    'testmonial_date' => "Testimonial's details",
    'job' => 'Job',
    'comment' => 'Client feedback',

    'blogs' => 'Blogs',
    'blogs_list' => 'Blog List',
    'create_blog' => 'Create Blog',
    'update_blog' => 'Update Blog',
    'blog_date' => "Blog's details",
    'created_at' => 'Created at',

    'teams' => 'Our team',
    'teams_list' => 'Our Team',
    'create_team' => 'New member',
    'update_team' => 'Update Member',
    'team_data' => "Member's details",
    'team_desc' => 'About ... ',



    'create_partner' => 'New partner',
    'update_partner' => 'Update partner',
    'partner_data' => "Partner's details",

    'mobile' => "Mobile Number",

    'settings' => 'Main settings',
    'fb' => 'Facebook',
    'tw' => 'Twitter',
    'pin' => 'Pinterest',
    'yt' => 'Youtube',
    'video' => 'Video',
    'gp' => 'Google+',
    'ins' => 'Instagram',
    'linked' => 'LinkedIn',
    'start' => 'Year\'s Start ',
    'end' => 'Year\'s End',
    'count1' => ' Counter 1',
    'count2' => ' Counter 2',
    'count3' => ' Counter 3',
    'count4' => ' Counter 4',
    'order' => ' Item Order',


    'are_sure' => 'Are you sure?',
    'delete_alert' => 'Your will not be able to recover this imaginary file or related items !',
    'cancel' => 'Cancel',
    'done' => 'Done',
    'yes' => 'Yes',
    'no' => 'No',
    'ok' => 'OK',
    'month_price' => 'Month Price',
    'semester_price' => 'Semester Price',
    'year_price' => 'Year Price',

    'welcome_back' => 'Welcome Back',
    'login' => 'Login',
    'home' => 'Home',
    'no_levels' => 'There isn\'t Levels exist',
    'edit_teacher' => 'Edit Teacher',
    'subscribers' => 'Subscribers',
    'subscribers_list' => 'Subscribers List',
    'subscribe_at' => 'Subscribe at  ',
    'messages' => 'Messages',
    'messages_list' => 'Message List',

    'congrats'=>'Congratulations ',
    'see_all'=>'See All',
    'students_count'=>'Our students',
    'students_desc'=>'You have done 57.6% more sales today.',
    'latest_updates'=>'Latest updates',

    'exams'=>'Exams',
    'month'=>'This Month',
    'week'=>'This week',




    'is_educations'=>'Show Eduction Types ?',
    'is_knowledge'=>'Show Knowledge bank?',
    'is_bank'=>'Show Questions Bank ?',
    'is_main'=>'Show Main section ?',
    'is_contacts'=>'Show Contact info ?',
    'is_contactus'=>'Show Contact form ?',
    'is_map'=>'Show Map ?',





    'level_price'=>'Level Price ',
    'level_offer'=>'Offer %',

    'processing'=>'Processing',
    'search'=>'Search',
    'loadingRecords'=>'Loading ...',
    'show'=>'Show',
    'entries'=>'Entries',
    'emptyTable'=>'Empty Table',
    'zeroRecords'=>'There is no data to show',
    'showing'=>'Showing',
    'to'=>'To',
    'from'=>'Of',
    'date_from'=>'From',
    'first'=>'First',
    'previous'=>'Previous',
    'next'=>'Next',
    'last'=>'Last',


    'select_video'=>'Select Video',
    'select_image'=>'Select Image',

    'drop_upload'=>' Drop Files Here To Upload',
    'file_details'=>'File Details',
    'Upload Files'=>'Upload Files',
    'description'=>'Description',
    'delete_video'=>'Delete Video',

    'brand'=>'Car Brand',
    'country'=>'Country',
    'government'=>'Government',
    'currency_name'=>'Currency Name',
    'currency_code'=>'Currency Code',
    'is_sale'=>'Show If car is for Sale ?',
    'is_stolen'=>'Show If car is Stolen ?',
    'is_photo'=>'Show Attachment photo requirement ?',
    'is_message'=>'Show Message Replay ?',
    'message'=>'Message',
    'currency'=>'Currency',
    'select_country' => 'Select Country ',
    'select_government' => 'Select Government ',
    'select_city' => 'Select City ',
    'location' => 'Location',
    'role'=>'Role',
    'categories' => 'Categories',
    'categories_list' => 'Categories List ',
    'create_category' => 'Create Category',
    'edit_category' => 'Edit Category',
    'category' => 'Category',
    'objects'=>'Objects',
    'objects_list'=>'Objects List',
    'objects_add'=>'Objects Add',


    'events' => 'Events',
    'events_list' => 'Events List ',
    'create_event' => 'Create Event',
    'edit_event' => 'Edit Event',
    'event' => 'Event',
    'duration_in_hour' => 'Duration In Hour',
    'is_public' => 'Is Public',
    'cover' => 'Cover',
    'user' => 'User',
    'city' => 'City',
    'start_date' => 'Start Date',
    'end_date' => 'End Date',
    'update_event' => 'Update Event',
    'invite_price'=>'Invite Price',
    'invite_price_point'=>'Invite Price by Points',
    'phone_code'=>'Phone Code',
    'slider_background'=>'Slider Background',
    'basic_section_image'=>'Basic Section Image',
    'prices'=>'Price List',
    'price_in'=>'Price In',
    'price_points'=>'Price In Points In',
    'packages' => 'Pricing plans(Packages)',
    'packages_list' => 'Packages List ',
    'create_package' => 'Create Package',
    'edit_package' => 'Edit Package',
    'package' => 'Package',
    'no_events'=>'Events Count',
    'no_invitations'=>'Invitations/Event Count',
    'promo_codes'=>'Promotions',
    'promo_codes_list'=>'Promotions list',
    'promo_code'=>'Promo code',
     'offer'=>'Offer',
    'is_limited'=>'Is Limited ?',
     'is_expired'=>'Is Expired ?',
    'create_promo_code'=>'Create PromoCode',

    'usage_limit'=>'Usage Limit',
    'offer_type'=>'Offer Type',
    'value'=>'Value',
    'expire_date'=>'Expire Date',
    'condition_type'=>'Condition Type',
    'condition_operation'=>'Condition Operation',
    'about_app'=>'About App',
    'key_word'=>'Key Word',
    'is_home'=>'Is Home',
    'subject'=>'Subject',
    'yes'=>'Yes',
    'no'=>'No',
    'update_feature'=>'Update Feature',
    'select_package'=>'Select Package',
    'offers'=>'Offers',
    'offers_list'=>'Offers List',
    'create_offer'=>'Create Offer',
    'bio'=>'Bio',
    'linkedin'=>'Linkedin',
    'update'=>'Update',
    'ads'=>'Advertisers',
    'ads_list'=>'Advertisers List',
    'create_ad'=>'Create Advertiser',
    'work_data'=>'Step Data',
    'create_work'=>'Create Step',
    'only_svg'=>'Only Svg',
    'partners' => 'Partners',
    'partners_list' => 'Partners List',
    'works' => 'How To work',
    'works_list' => 'Steps List',
     'slider_offer' => 'Slider Offer',
    'blog'=>"Blog",
    'publish_date' => "Publish date",
     'blogs_background'=>'Blogs Background',
    'pages_background'=>'Pages Background',
    'inactive'=>'Not Active',
    'template'=>'Page Template',
    //'translation'=>'Translation',
    'jobs'=>'Jobs',
    'jobs_list'=>'jobs List',
    'create_job'=>'Create job',
    'edit_job'=>'Edit job',
    'select_job'=>'Select job',
    'is_package'=>'Is Package',
    'select_category'=>'Select Category',
    'profile_updated'=>'Profile Updated',
    'update_profile'=>'Update Profile',
    'password_confirmation'=>'Password Confirmation',
    'password_updated'=>'Password Updated',
    'evente_duplicated'=>'Event Duplicated',
    'free_events'=>'Free Events',
    'free_event_invitations'=>'No. invitations Free Event',
    'testimonials'=>'Testimonials',
    'no_action'=>'No Action',
    'going'=>'Going',
    'maybe'=>'Maybe',
    'not_going'=>'Not Going',
    'attended'=>'Attended',
    'invitation_resend_successfully'=>'Invitation Resended',
    'upload'=>'Upload',
    'testimonials_list'=>'Testimonials List',
    'create_testimonial'=>'Create Testimonial',
    'register_reward_points'=>'Register Reward Points',
    'package_reward_points'=>'Package Reward Points',
    'invitations'=>'Invitations',
    'total'=>'Total',
    'orders'=>'Orders',
    'should_accept'=>'Should Accept Attend Requests',
    'affiliate_settings'=>'Affiliate Settings',
    'percent'=>'Percent',
    'percent'=>'Affiliate Settings',
    'registered_at'=>'Registered At',
    'go_users'=>'Go Users',
    'go_orders'=>'Go Orders',
    'go_events'=>'Go Events',
    'affilators'=>'Affilators',
    'go_affilators'=>'Go Affilators',
    'affilators'=>'Affilators',
    'payment_status'=>'Payment Status',
    'amount'=>'Amount',
    'registered_at'=>'Registered At',
    'affilators_transactions'=>'Affilators Transactions',
    'transactions_list'=>'Affilators Transactions List',
    'gallery'=>'Gallery',
    'filter'=>'Filter',
    'orders_list'=>'Orders List',
    'date'=>'Date',
    'is_terms'=>'Is Terms',
    'is_policy'=>'Is Policy',

    'logo'=>'logo',
    'ad_categories_background'=>'Ad Categories Background',
    'job_advertisers_background'=>'Job Advertisers Background',
    'contact_background'=>'Contact Background',
    'tab_icon'=>'Tab Icon',
    'affilate'=>'Affilate',
    'wallet'=>'Wallet',
    'is_multiple_scan'=>'Is Multiple Scan',
    'export'=>'Export',
    'create_gallery'=>'Create Gallery',
    'no_action'=>'No Action',
    'owner'=>'Event Owner',
    'package_expire_after'=>'Package Expire After',
    'days'=>'Days',
    'event_name'=>'Event Name',
    'is_default_country'=>'Is Default Country',
    'sent_invitations'=>'Sent Invitations',
    'available_invitations'=>'Available Invitations',
    'invitations_counters'=>'Invitations Counters',
    'scanned'=>'Scanned Qr',
    'qr'=>'Qr',
    'scan_count'=>'Scan Count',
    'location_url'=>'Location Url',
    'you_have_free_packacge_we_will_open_paid_packages_soon'=>'You Have Free Packacge We Will Open Paid Packages Soon',
];

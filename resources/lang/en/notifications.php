<?php

return [
    'admin_created_successfully'=>'New admin added successfully',
    'admin_updated_successfully'=>'Admin updated successfully',
    'admin_deleted_successfully'=>'Admin deleted successfully',

    'city_created_successfully'=>'New city added successfully',
    'city_updated_successfully'=>'City updated successfully',
    'city_deleted_successfully'=>'City deleted successfully',


    'country_created_successfully'=>'New country added successfully',
    'country_updated_successfully'=>'Country updated successfully',
    'country_deleted_successfully'=>'Country deleted successfully',


    'government_created_successfully'=>'New government added successfully ',
    'government_updated_successfully'=>'Government updated successfully',
    'government_deleted_successfully'=>'Government deleted successfully',

    'language_created_successfully'=>'New Language added successfully',
    'language_updated_successfully'=>'Language Updated successfully',
    'language_deleted_successfully'=>'Language deleted successfully',

    'role_created_successfully'=>'New Role added successfully',
    'role_updated_successfully'=>'Role updated successfully',
    'role_deleted_successfully'=>'Role deleted successfully',

    'category_created_successfully'=>'New Category added successfully',
    'category_updated_successfully'=>'Category updated successfully',
    'category_deleted_successfully'=>'Category deleted successfully',


    'user_created_successfully'=>'New User added successfully',
    'user_updated_successfully'=>'User updated successfully',
    'user_deleted_successfully'=>'User deleted successfully',

    'feature_created_successfully'=>'New Feature added successfully',
    'feature_updated_successfully'=>'Feature updated successfully',
    'feature_deleted_successfully'=>'Feature deleted successfully',

    'page_created_successfully'=>'New Page added successfully',
    'page_updated_successfully'=>'Page updated successfully',
    'page_deleted_successfully'=>'Page deleted successfully',

    'blog_created_successfully'=>'New Blog Added successfully',
    'blog_updated_successfully'=>'Blog updated successfully',
    'blog_deleted_successfully'=>'Blog deleted successfully',

    'subscribe_deleted_successfully'=>'Subscriber deleted successfully',
    'message_deleted_successfully'=>'Message deleted successfully',

    'package_created_successfully'=>'New Package Added successfully',
    'package_updated_successfully'=>'Package updated successfully',
    'package_deleted_successfully'=>'Package deleted successfully',


 ];

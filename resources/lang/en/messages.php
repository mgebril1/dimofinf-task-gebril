<?php

return [
    'create_admin_success'=>'New admin added successfully',
    'update_admin_success'=>'Admin updated successfully',
    'delete_admin_success'=>'Admin deleted successfully',
    'update_settings_success'=>'Main settings updated successfully',
    'create_blog_success'=>'a Vew blog created successfully',
    'update_blog_success'=>'a blog updated successfully',
    'delete_blog_success'=>'blog deleted successfully',
    'create_education_success'=>'Education Level created successfully',
    'update_education_success'=>'Education Level updated successfully',
    'delete_education_success'=>'Education Level deleted successfully',

    'create_language_success'=>'Language created successfully',
    'update_language_success'=>'Language updated successfully',
    'delete_language_success'=>'Language deleted successfully',

    'create_level_success'=>'Level created successfully',
    'update_level_success'=>'Level updated successfully',
    'delete_level_success'=>'Level deleted successfully',
    'delete_message_success'=>'Message deleted successfully',

    'create_page_success'=>'Page created successfully',
    'update_page_success'=>'Page updated successfully',
    'delete_page_success'=>'Page deleted successfully',

    'create_parent_success'=>'User created successfully',
    'update_parent_success'=>'User updated successfully',
    'delete_parent_success'=>'User deleted successfully',

    'create_role_success'=>'Role created successfully',
    'update_role_success'=>'Role updated successfully',
    'delete_role_success'=>'Role deleted successfully',

    'create_partner_success'=>'Partner created successfully',
    'update_partner_success'=>'Partner updated successfully',
    'delete_partner_success'=>'Partner deleted successfully',


    'create_school_success'=>'Partner created successfully',
    'update_school_success'=>'Partner updated successfully',
    'delete_school_success'=>'Partner deleted successfully',

    'create_subject_success'=>'Subject created successfully',
    'update_subject_success'=>'Subject updated successfully',
    'delete_subject_success'=>'Subject deleted successfully',


    'create_student_success'=>'Student created successfully',
    'update_student_success'=>'Student updated successfully',
    'delete_student_success'=>'Student deleted successfully',

    'update_unit_success'=>'Unit updated successfully',
    'delete_unit_success'=>'Unit deleted successfully',


    'create_topic_success'=>'Topic created successfully',
    'update_topic_success'=>'Topic updated successfully',
    'delete_topic_success'=>'Topic deleted successfully',



    ];

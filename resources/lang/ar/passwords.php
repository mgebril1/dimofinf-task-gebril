<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => ' كلمة السر علي اﻷقل 6 حروف وتكون مماثله مع تأكيد كلمة السر.',
    'reset' => 'تم حديث كلمة السر الخاصة بك.',
    'sent' => ' تم إرسال رساله عبر اﻹيميل لتغيير كلمة السر',
    'token' => 'الكود غير صحيح.',
    'user' => "ﻻيوجد مستخدم بهذا البريد اﻹلكتروني",

];

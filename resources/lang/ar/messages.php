<?php

return [
    'create_admin_success'=>'تم اضافة مدير جديد',
    'update_admin_success'=>'تم تعديل بيانات المدير بنجاح',
    'delete_admin_success'=>'تم حذف مدير',
    'update_settings_success'=>'تم تعديل اعدادات الموقع',
    'create_blog_success'=>'تم اضافة مقال جديد',
    'update_blog_success'=>'تم تعديل المقال بنجاح',
    'delete_blog_success'=>'تم مسح مقال',
    'create_education_success'=>'تم اضافة نوع تعليمى جديد',
    'update_education_success'=>'تم تعديل نوع التعليم ',
    'delete_education_success'=>'تم حذف  نوع التعليم',

    'create_language_success'=>'تم اضافة لغة جديدة',
    'update_language_success'=>'تم تعديل اللغة',
    'delete_language_success'=>'تم حذف اللغة',

    'create_level_success'=>'تم اضافه مستوى تعليمى جديد',
    'update_level_success'=>'تم تعديل المستوى التعليمى',
    'delete_level_success'=>'تم حذف المستوى التعليمى',
     'delete_message_success'=>'تم حذف الرسالة',

    'create_page_success'=>'تم اضافة صفحة جديدة',
    'update_page_success'=>'تم تعديل تفاصيل الصفحة',
    'delete_page_success'=>'تم حذف الصفحة من الموقع',

    'create_parent_success'=>'تم اضافة ولى امر جديد',
    'update_parent_success'=>'تم تعديل بيانات المستخدم',
    'delete_parent_success'=>'تم حذف ولى الامر',

    'create_role_success'=>'تم اضافة مجموعة صلاحيات ',
    'update_role_success'=>'تم تعديل الصلاحيات',
    'delete_role_success'=>'تم حذف المجموعة',

    'create_partner_success'=>'تم اضافة شريك جديد',
    'update_partner_success'=>'تم تعديل بيانات الشريك',
    'delete_partner_success'=>'تم حذف الشريك',


    'create_school_success'=>'تم اضافه مدرسة جديدة',
    'update_school_success'=>'تم تعديل بيانات المدرسة',
    'delete_school_success'=>'تم حذف المدرسة',

    'create_subject_success'=>'تم اضافة ماده دراسية جديدة',
    'update_subject_success'=>'تم تعديل المادة الدراسية',
    'delete_subject_success'=>'تم حذف الماده الدراسية',


    'create_student_success'=>'تم اضافة طالب جديد',
    'update_student_success'=>'تم تعديل بيانات الطالب',
    'delete_student_success'=>'تم حذف الطالب',

    'update_unit_success'=>'تم تعديل الوحدة',
    'delete_unit_success'=>'تم حذف الوحدة',

    'create_topic_success'=>'تم اضافة درس جديد',
    'update_topic_success'=>'تم تعديل بيانات الدرس',
    'delete_topic_success'=>'تم حذف الدرس',

    ];

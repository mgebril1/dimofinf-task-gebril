<?php

return [
    'admin_created_successfully'=>'تم اضافة مدير جديد',
    'admin_updated_successfully'=>'تم تعديل بيانات المدير بنجاح',
    'admin_deleted_successfully'=>'تم حذف مدير',

    'city_created_successfully' => 'تم اضافة منطقة جديدة بنجاح',
    'city_updated_successfully' => 'تم تعديل المنطقة بنجاح',
    'city_deleted_successfully' => 'تم حذف المنطقة بنجاح',

    'country_created_successfully' => 'تم اضافة بلد جديده بنجاح',
    'country_updated_successfully' => 'تم تعديل البلد بنجاح',
    'country_deleted_successfully' => 'تم حذف البلد بنجاح',

    'government_created_successfully' => 'تم اضافة محافظه جديده بنجاح',
    'government_updated_successfully' => 'تم تعديل المحافظة بنجاح',
    'government_deleted_successfully' => 'تم حذف المحافظة بنجاح',

    'language_created_successfully' => 'تم اضافة لغة جديدة بنجاح',
    'language_updated_successfully' => 'تم تعديل اللغه بنجاح',
    'language_deleted_successfully' => 'تم حذف اللغة بنجاح',

    'role_created_successfully' => 'تم اضافة صلاحية جديدة',
    'role_updated_successfully' => 'تم تعديل الصلاحية بنجاح',
    'role_deleted_successfully' => 'تم حذف الصلاحيه بنجاح',

    'category_created_successfully' => 'تم اضافة نوع جديدة',
    'category_updated_successfully' => 'تم تعديل النوع بنجاح',
    'category_deleted_successfully' => 'تم حذف النوع بنجاح',

    'user_created_successfully'=>'تم اضافة مستخدم جديد بنجاح',
    'user_updated_successfully'=>'تم تعديل بيانات المستخدم بنجاح',
    'user_deleted_successfully'=>'تم حذف المستخدم بنجاح',


    'blog_created_successfully'=>'تم اضافة مقال جديد بنجاح',
    'blog_updated_successfully'=>'تم تعديل بيانات المقال بنجاح',
    'blog_deleted_successfully'=>'تم حذف المقال بنجاح',

    'feature_created_successfully'=>'تم اضافة ميزة جديدة',
    'feature_updated_successfully'=>'تم تعديل الميزة بنجاح',
    'feature_deleted_successfully'=>'تم حذف الميزة بنجاح',

    'page_created_successfully'=>'تم اضافة صفحة جديدة بنجاح',
    'page_updated_successfully'=> 'تم تعديل الصفحة بنجاح',
    'page_deleted_successfully'=>'تم حذف الصفحه بنجاح',

    'subscribe_deleted_successfully'=>'تم حذف المشترك بنجاح',
    'message_deleted_successfully'=>'تم حذف الرسالة بنجاح',


    'package_created_successfully'=>'تم اضافة باقة جديدة',
    'package_updated_successfully'=>'تم تعديل الباقة بنجاح',
    'package_deleted_successfully'=>'تم حذف الباقة بنجاح',


];




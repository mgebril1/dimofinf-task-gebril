<?php

return [
    'create_admin' => 'اضاف مدير جديد',
    'update_admin' => 'عدل بيانات المدير',
    'delete_admin' => 'مسح مدير',

    'create_role' => 'أضاف صلاحية جديدة',
    'update_role' => 'عدل ع تفاصيل الصلاحية ',
    'delete_role' => 'مسح صلاحيه',

    'create_student' => 'اضاف طالب جديد',
    'update_student' => 'قام بتعديل بيانات',
    'delete_student' => 'قام بمسح طالب ',

    'create_school' => 'اضاف مدرسة جديدة',
    'update_school' => 'قام بتعديل بيانات ',
    'delete_school' => 'قام باضافة مدرسه',

    'create_teacher' => 'اضاف مدرس جديد',
    'update_teacher' => 'قام بتعديل بيانات',
    'delete_teacher' => 'قام بمسح مدرس ',

    'create_parent' => 'اضاف ولى امر',
    'update_parent' => 'قام بتعديل بيانات',
    'delete_parent' => 'قام بمسح ولى امر',

    'create_education' => 'اضاف نوع جديد للتعليم',
    'update_education' => 'تم تعديل بيانات ',
    'delete_education' => 'تم حذف نوع تعليم ',
];
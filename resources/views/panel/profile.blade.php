@extends('panel.layout.master')
@section('content')

  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white me-2">
        <i class="mdi mdi-account"></i>
          </span> {{ \App\Help::translate('update_profile',$local) }}
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}/panel">{{ \App\Help::translate('dashboard',$local) }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ \App\Help::translate('update_profile',$local) }}</li>
      </ul>
    </nav>
  </div>

<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      {!! Form::model($row,['route' => 'panel.update_profile', 'class'=>'forms-sample row' ,'files' => true]) !!}
        <div class="col-9">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('user_account_name',$local)}}</label>
                {{ Form::text('name', isset($row) ? $row->name : '', ['class'=>' form-control' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'required' => 'required', 'placeholder'=>trans('dashboard.name')]) }}
                <small class="text-danger">{{ $errors->first('name') }}</small>
              </div>
            </div>
            @if(auth()->user()->is_social !=1)
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('email',$local)}}</label>
                {{ Form::email('email', isset($row) ? $row->email : '', ['class'=>' form-control' . ($errors->has('email') ? 'redborder' : '')  , 'id' => 'email', 'required' => 'required', 'placeholder'=>trans('dashboard.email')]) }}
                <small class="text-danger">{{ $errors->first('email') }}</small>
              </div>
            </div>
            @endif
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('mobile',$local)}}</label>
                {{ Form::text('mobile', isset($row) ? $row->mobile : '', ['class'=>' form-control' . ($errors->has('mobile') ? 'redborder' : '')  , 'onkeypress'=>'return onlyNumberKey(event)','id' => 'mobile', 'placeholder'=>trans('dashboard.mobile')]) }}
                <small class="text-danger">{{ $errors->first('mobile') }}</small>
              </div>
            </div>
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('address',$local)}}</label>
                {{ Form::text('address', isset($row) ? $row->address : '', ['class'=>' form-control' . ($errors->has('address') ? 'redborder' : '')  , 'id' => 'address', 'placeholder'=>trans('dashboard.address')]) }}
                <small class="text-danger">{{ $errors->first('address') }}</small>
              </div>
            </div> -->
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('country',$local)}}</label>
                {{ Form::select('country_id',$countries, isset($row) ? $row->country_id : [], ['class'=>' form-control' . ($errors->has('countries') ? 'redborder' : '')  , 'id' => 'countries', 'required' => 'required','placeholder'=> trans('dashboard.select_country')]) }}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('city',$local)}}</label>
                {{ Form::select('city_id',$cities, isset($row) ? $row->city_id : [], ['class'=>' form-control' . ($errors->has('cities') ? 'redborder' : '')  , 'id' => 'cities', 'required' => 'required','placeholder'=> trans('dashboard.select_city')]) }}
                <small class="text-danger">{{ $errors->first('city_id') }}</small>
              </div>
            </div> -->
          </div>
        </div>
        <div class="col-3">
          <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
              <div class="form-group">
                @if(isset($row->image))
                  <img src="{{$row->image}}" id="picture" style="width: 205px;height: 205px;border-radius: 50%;" alt="{{$row->image}}">
                @else
                  <img src="/assets/images/users/default.png" id="picture" style="width: 205px;height: 205px;border-radius: 50%;" alt="">
                @endif
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                {{ Form::file('image', ['class'=>'file-upload-default' . ($errors->has('image') ? 'redborder' : '')  , 'id' => 'image', 'placeholder'=>'image']) }}
                <div class="input-group col-xs-12 w-100">
                  <span class="input-group-append w-100">
                    <button class="file-upload-browse btn btn-gradient-primary w-100" type="button">{{\App\Help::translate('upload',$local)}}</button>
                    <input type="text" class="form-control file-upload-info border-0 " disabled placeholder="">
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-6">
          <button type="submit" class="btn btn-gradient-primary me-2">{{ \App\Help::translate('submit',$local) }}</button>
          <a href="{{route('panel.index')}}" class="btn btn-light">{{ \App\Help::translate('cancel',$local) }}</a>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection

@extends('panel.layout.master')
@section('content')

    <div class="page-header">
        <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white me-2">
        <i class="mdi mdi-account"></i>
          </span> {{ \App\Help::translate('profile',$local) }}
        </h3>
        <nav aria-label="breadcrumb">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url($localization.'/')}}/panel">{{ \App\Help::translate('dashboard',$local) }}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ \App\Help::translate('profile',$local) }}</li>
            </ul>
        </nav>
    </div>

    <section class="section about-section gray-bg" id="about">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="about-avatar">
                            @if(isset($user->image))
                                <img src="{{$user->image}}" id="picture" style="width:100%;border-radius: 50%;" alt="{{$user->image}}">
                            @else
                                <img src="/assets/images/users/default.png" id="picture" style="width:100%;border-radius: 50%;" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="about-text go-to">
                            <h3 class="dark-color">{{ $user->name }} </h3>
                            <p>{{ $user->bio }}</p>
                            <div class="row about-list">
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>{{ \App\Help::translate('email',$local) }}</label>
                                        <p style="color: cadetblue;">{{ $user->email }}</p>
                                    </div>
                                    <div class="media">
                                        <label>{{ \App\Help::translate('mobile',$local) }}</label>
                                        <p style="color: cadetblue;">{{ $user->mobile }}</p>
                                    </div>
                                    <div class="media">
                                        <label>{{ \App\Help::translate('city',$local) }}</label>
                                        <p style="color: cadetblue;">{{ isset($user->city) ? $user->city->page($lang->id)->name : "--" }} - {{ isset($user->country) ? $user->country->page($lang->id)->name : "--" }}</p>
                                    </div>
                                    <div class="media">
                                        <label>{{ \App\Help::translate('address',$local) }}</label>
                                        <p style="color: cadetblue;">{{ $user->address }}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="media">
                                        <label>{{ \App\Help::translate('free_events',$local) }}</label>
                                        <p style="color: cadetblue;">{{ $user->free_events }}</p>
                                    </div>
                                    <div class="media">
                                        <label>{{ \App\Help::translate('current_package',$local) }}</label>
                                        @if(user()->current_package()->page($lang->id))
                                            <p style="color: cadetblue;">{{ $user->current_package()->page($lang->id)->name }}</p>
                                        @else
                                            <p style="color: cadetblue;">{{ $user->current_package()->page(1)->name }}</p>
                                        @endif
                                    </div>
                                    <div class="media">
                                        <label>{{ \App\Help::translate('invitation_link',$local) }}</label>
                                        <a class="text-secondary text-small" style="cursor: pointer;" onclick="copyToClipboard('#invitaion_link',$(this))">
                                            <p id="invitaion_link" >{{url($localization .'/register/'.auth()->user()->registration_code)}}</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-3">
            <a class="btn btn-gradient-primary btn-fw mb-2" href="{{url('panel/edit-profile')}}">
                <i class="mdi mdi-account-edit me-2"></i> {{ \App\Help::translate('panel_edit_profile',$local)}} </a>
            <a class="btn btn-gradient-primary btn-fw mb-2" href="{{url('panel/edit-password')}}">
                <i class="mdi mdi-cached me-2"></i> {{ \App\Help::translate('change_password',$local)}} </a>
        </div>
    </section>
@endsection
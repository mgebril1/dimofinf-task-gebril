
<script src=" {{  mix('/assets/frontend/panel/js/manifest.min.js') }}"></script>
<script src=" {{  mix('/assets/frontend/panel/js/jquery.min.js') }}"></script>
<script src=" {{  mix('/assets/frontend/panel/js/vendor.min.js') }}"></script>

<script src=" {{  asset('assets/panel/vendors/js/vendor.bundle.base.js') }}"></script>
<script src=" {{  asset('assets/panel/vendors/chart.js/Chart.min.js') }}"></script>
<script src=" {{  asset('assets/panel/js/jquery.cookie.js') }}"></script>
<script src=" {{  asset('assets/panel/js/off-canvas.js') }}"></script>
<script src=" {{  asset('assets/panel/js/hoverable-collapse.js') }}"></script>
<script src=" {{  asset('assets/panel/js/misc.js') }}"></script>
<script src=" {{  asset('assets/panel/js/dashboard.js') }}"></script>
<script src=" {{  asset('assets/panel/js/todolist.js') }}"></script>

{{--<script src=" {{  asset('assets/panel/js/select2.min.js') }}"></script>--}}
<script src=" {{  asset('assets/panel/js/sweetalert2.all.min.js') }}"></script>

<script src=" {{ mix('/assets/frontend/panel/js/app.min.js') }}"></script>
<script src=" {{ asset('assets/backend') }}/assets/js/ajax.js"></script>
<script src="{{ asset('assets/backend') }}/app-assets/vendors/js/forms/select/select2.min.js" defer></script>


@if(session('success'))
<script>
  Swal.fire({
        title: "{{ session('success')}}",
        text: "",
        type: "success",
        confirmButtonText: "{{ trans('dashboard.ok') }}",
    }).then(function (result) {
        if (data.reload == 0) {
            window.history.back();
        } else {
            window.location.reload();
        }
    });
</script>
@endif


@if(session('error'))
<script>
  Swal.fire({
        title: "{{ session('error')}}",
        text: "",
        type: "error",
        confirmButtonText: "{{ trans('dashboard.ok') }}",
    }).then(function (result) {
        if (data.reload == 0) {
            window.history.back();
        } else {
            window.location.reload();
        }
    });
</script>
@endif


@yield('scripts')
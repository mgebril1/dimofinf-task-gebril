<!--partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ url('/panel') }}">
          <span class="menu-title">Dashboard</span>
          <i class="mdi mdi-home menu-icon"></i>
        </a>
      </li>
      <li class="nav-item active">
        <a href="{{route('panel.posts.index')}}" class="nav-link" >
          <span class="menu-title">Posts</span>
          <i class="mdi mdi-auto-fix menu-icon"></i>
        </a>
      </li>
      <li class="nav-item sidebar-actions">
        <span class="nav-link">
          <a style="width: 100%;max-width: 180px;" href="{{route('panel.posts.create')}}" class="btn btn-block btn-lg btn-gradient-primary mt-4">+ Add Post</a>
        </span>
      </li>
    </ul>
  </nav>
<!-- partial -->
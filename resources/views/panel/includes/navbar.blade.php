<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 d-flex flex-row fixed-top">
  <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
    <a class="navbar-brand brand-logo" href="index.html"><img src="{{asset('logo.png')}}" alt="logo" /></a>
    <a class="navbar-brand brand-logo-mini" href="index.html"><img src="{{asset('logo.png')}}" alt="logo" /></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-stretch">
    <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
      <span class="mdi mdi-menu"></span>
    </button>
    <div class="search-field d-none d-md-block">

    </div>
    <ul class="navbar-nav navbar-nav-right">
      <li class="nav-item nav-profile dropdown">
        <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
          <!-- <div class="nav-profile-img">
            <img src="{{asset(Auth::user()->image)}}" alt="image">
            <span class="availability-status online"></span>
          </div> -->
          <div class="nav-profile-text">
            <p class="mb-1 text-black">{{ auth()->user()->name }}</p>
          </div>
        </a>
        <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">


          <div class="card">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="about-text go-to">
                    <div class="row about-list">
                      <div class="col-md-12">
                        <div class="media d-flex flex-column">
                          <label class="d-flex"><i class="mdi mdi-account me-2 text-primary"></i> Name</label>
                          <p style="color: cadetblue;">{{ user()->name }}</p>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="media d-flex flex-column">
                          <label class="d-flex"><i class="mdi mdi-email me-2 text-primary"></i> Email</label>
                          <p style="color: cadetblue;">{{ user()->email }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="dropdown-divider"></div>
          @if(auth()->user()->is_social!=1)
          <a class="dropdown-item" href="{{url('/panel/edit-profile')}}">
            <i class="mdi mdi-account me-2 text-primary"></i>Edit profile
          </a>
          <a class="dropdown-item" href="{{url('/panel/edit-password')}}">
            <i class="mdi mdi-textbox-password me-2 text-primary"></i> Change Password
          </a>
          @endif
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{url('logout')}}">
            <i class="mdi mdi-logout me-2 text-primary"></i> Logout
          </a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="mdi mdi-menu"></span>
    </button>
  </div>
</nav>
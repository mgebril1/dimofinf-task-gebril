    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dimofinif | {{ Auth::User()->name }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@500&display=swap" rel="stylesheet">
    <style>
        *:not(i){
            font-family: 'Cairo', sans-serif !important;
        }
        #paymentErrors{
            color: #eb6141;
            background-color: #ffebeb;
            padding: 6px;
            border-radius: 7px;
            display: block;
            margin-bottom: 15px;
        }
        
    </style>
    <link rel="stylesheet" href="{{asset('assets/panel/')}}/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="{{asset('assets/panel/')}}/assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/panel/')}}/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/panel/')}}/assets/images/favicon.ico" />
    

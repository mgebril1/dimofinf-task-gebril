  <footer class="footer">
    <div class="container-fluid d-flex justify-content-between">
      <span class="text-muted d-block text-center text-sm-start d-sm-inline-block">Copyright © <a href="https://staging.eventiqr.com/" target="_blank">Mahmoud Gebril</a>{{date("Y")}} </span>
    </div>
  </footer>

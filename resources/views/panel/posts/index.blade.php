@extends('panel.layout.master')
@section('content')
  <div class="page-header">
    <h3 class="page-title">

      <span class="page-title-icon bg-gradient-primary text-white me-2">
        <i class="mdi mdi-home"></i>
      </span> Dimofinif Task
    </h3>

    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">
          <span></span>Posts <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
        </li>
      </ul>
    </nav>
  </div>
  <div class="row">

    <div class="col-lg-12 stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Posts</h4>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th> # </th>
                <th> Title </th>
                <th> Description </th>
                <th> Contact Number </th>
                <!-- <th> Deadline </th> -->
              </tr>
            </thead>
            <tbody>
              @foreach($posts as $post)
                <tr class="table">
                  <td> {{$post->id}} </td>
                  <td> {{$post->title}} </td>
                  <td> {{substr($post->desc,0,50)}}..... </td>
                  <td> {{$post->contact_number}} </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  {{ $posts->links() }}
@endsection
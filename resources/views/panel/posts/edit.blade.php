@extends('panel.layout.master')
@section('content')

  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white me-2">
        <i class="mdi mdi-auto-fix"></i>
            </span> Create Post
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/panel')}}"> Index</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit post</li>
      </ul>
    </nav>
  </div>

<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <p><strong>Opps Something went wrong</strong></p>
                    <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </ul>
                </div>
            @endif
      {!! Form::model($row,['route' => ['panel.posts.update',$row->id],'method' => 'PATCH' , 'files' => true]) !!}
                 @include('panel.posts.form', ['btn' => \App\Help::translate('save',$local), 'classes' => 'btn-primary btn-xs btn-block'])
          {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection
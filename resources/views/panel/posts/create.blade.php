@extends('panel.layout.master')
@section('content')
<div class="page-header">
  <h3 class="page-title">
    <span class="page-title-icon bg-gradient-primary text-white me-2">
      <i class="mdi mdi-auto-fix"></i>
          </span> Create Post
  </h3>
  <nav aria-label="breadcrumb">
    <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{url('/panel')}}"> Index</a></li>
      <li class="breadcrumb-item active" aria-current="page">create post</li>
    </ul>
  </nav>
</div>
<div class="col-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <p class="card-description"> New Post</p>
      {!! Form::open(['route' => 'panel.posts.store', 'class'=>'forms-sample' ,'files' => true]) !!}
            @include('panel.posts.form', ['btn' => 'Save', 'classes' => 'btn-primary btn-xs btn-block'])
        {!! Form::close() !!}
    </div>
  </div>
</div>
@endsection
<div class="col-9">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputName1">Title</label>
        {{ Form::text('title', isset($row) ? $row->title : '', ['class'=>' form-control ' . ($errors->has('title') ? 'redborder' : '')  , 'id' => 'title', 'required' => 'required', 'placeholder'=>'Title']) }}
        <small class="text-danger">{{ $errors->first('title') }}</small>
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        <label for="exampleInputName1">Contact Number</label>
        {{ Form::text('contact_number', isset($row) ? $row->contact_number : '', ['class'=>' form-control ' . ($errors->has('contact_number') ? 'redborder' : '')  , 'onkeypress'=>'return onlyNumberKey(event)','id' => 'contact_number', 'placeholder'=>'Contact Number']) }}
        <small class="text-danger">{{ $errors->first('contact_number') }}</small>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label for="exampleInputName1">Description</label>
        {{ Form::textarea('desc', isset($row) ? $row->desc : '', ['class'=>' form-control ' . ($errors->has('desc') ? 'redborder' : '') ,'id' => 'desc', 'placeholder'=>'Description']) }}
        <small class="text-danger">{{ $errors->first('desc') }}</small>
      </div>
    </div>

    <div class="col-md-12">
       <div class="form-group">
          <label>File upload</label>
          <input type="file" name="image" class="file-upload-default">
          <div class="input-group col-xs-12">
            <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
            <span class="input-group-append">
              <button class="file-upload-browse btn btn-gradient-primary" type="button">Upload</button>
            </span>
          </div>
          <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>
    </div>
  </div>
</div>

<div class="col-6">
  <button type="submit" class="btn btn-gradient-primary me-2">Submit</button>
  <a href="{{route('panel.index')}}" class="btn btn-light">Cancel</a>
</div>
@section('scripts')
<script src="{{asset('assets/panel/')}}/assets/js/file-upload.js"></script>
@endsection
@extends('frontend.layout.master')
@section('content')
    <div id="page-banner-area" class="page-banner-area" style="background-image:url(/assets/frontend/images/hero_area/banner_bg.jpg)">
         <!-- Subpage title start -->
        <div class="page-banner-title">
            <div class="text-center">
               <h2>{{\App\Help::translate('edit_profile',$local)}}</h2>
               <ol class="breadcrumb">
                  <li>
                     <a href="#">{{\App\Help::translate('eventiqr',$local)}} /</a>
                  </li>
                  <li>
                    {{\App\Help::translate('edit_profile',$local)}}
                  </li>
               </ol>
            </div>
        </div><!-- Subpage title end -->
    </div><!-- Page Banner end -->

    <section class="ts-contact-form">
       <div class="container">
          <div class="row">
             <div class="col-lg-8 mx-auto">
                <h2 class="section-title text-center">
                   <span>{{\App\Help::translate('edit_profile_form_sub_title',$local)}}</span>
                   {{\App\Help::translate('edit_profile_form_title',$local)}}
                </h2>
             </div><!-- col end-->
          </div>
          <div class="row">
             <div class="col-lg-8 mx-auto">
                @if(session()->has('success'))
               <div style="position: relative;margin-top: 16px;">
                  <p style="color: aliceblue;bottom: 20px;position: absolute;background-color: #8fb58d;padding: 10px;border-radius: 9px;">
                         {{session()->get('success')}}
                  </p>
               </div>
                @endif
                <form action="{{url($local.'/'.country()->code.'/update-profile')}}" id="contact-form" class="contact-form" method="post">
                        @csrf
                   <div class="error-container"></div>
                   <div class="row">
                     <div class="col-md-6">
                         <div class="form-group">
                            <input class="form-control" placeholder="{{\App\Help::translate('name',$local)}}" name="name" id="name" type="text" value="{{$user->name}}" required="">
                         <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                            <input class="form-control form-control-email" placeholder="{{\App\Help::translate('email',$local)}}" name="email" id="email" type="email" value="{{$user->email}}" required="">
                         <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                     </div>
                     <div class="col-md-6">
                         <div class="form-group">
                            <input class="form-control form-control-phone" placeholder="{{\App\Help::translate('mobile',$local)}}" name="mobile" id="mobile" value="{{$user->mobile}}" required="" onkeypress="return onlyNumberKey(event)">
                         <small class="text-danger">{{ $errors->first('mobile') }}</small>
                        </div>
                     </div>

                     <div class="col-md-6">
                        <div class="form-group">
                           <input class="form-control" placeholder="{{\App\Help::translate('address',$local)}}" name="address" value="{{$user->address}}" id="address">
                        <small class="text-danger">{{ $errors->first('address') }}</small>
                        </div>
                     </div>
                     
                     <div class="col-md-6">
                        <div class="form-group">
                           <select class="form-control" name="country_id" onchange="getCitiesByCountry($(this))" required>
                              <option ></option>
                              @foreach($countries as $country)
                                 <option value="{{$country->id}}" @if($user->country_id == $country->id) selected @endif >{{$country->name}}</option>
                              @endforeach
                           </select>
                            
                         <small class="text-danger">{{ $errors->first('country_id') }}</small>
                        </div>
                     </div>

                     <div class="col-md-6">
                        <div class="form-group">
                           <select class="form-control" name="city_id" id="city_id">
                              <option></option>
                              @foreach($cities as $city)
                                 <option value="{{$city->id}}" @if($user->city_id == $city->id) selected @endif >{{$city->name}}</option>
                              @endforeach
                           </select>
                           <small class="text-danger">{{ $errors->first('city_id') }}</small>
                        </div>
                     </div>
                   </div>

                  <div class="text-center"><br>
                     <button class="btn" type="submit"> {{\App\Help::translate('update_profile_button_name',$local)}}</button>
                  </div>
                </form><!-- Contact form end -->
             </div>
          </div>
       </div>
       <div class="speaker-shap">
          <img class="shap1" src="{{asset('assets/frontend')}}/images/shap/home_schedule_memphis2.png" alt="">
       </div>
    </section>

@endsection

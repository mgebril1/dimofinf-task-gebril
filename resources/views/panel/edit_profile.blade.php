@extends('panel.layout.master')
@section('content')

  <div class="page-header">
    <h3 class="page-title">
      <span class="page-title-icon bg-gradient-primary text-white me-2">
        <i class="mdi mdi-account"></i>
          </span> {{ \App\Help::translate('update_profile',$local) }}
    </h3>
    <nav aria-label="breadcrumb">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('/')}}/panel">{{ \App\Help::translate('dashboard',$local) }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ \App\Help::translate('update_profile',$local) }}</li>
      </ul>
    </nav>
  </div>

<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
      {!! Form::model($row,['route' => 'panel.update_profile', 'class'=>'forms-sample row' ,'files' => true]) !!}
        <div class="col-9">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('name',$local)}}</label>
                {{ Form::text('name', isset($row) ? $row->name : '', ['class'=>' form-control' . ($errors->has('name') ? 'redborder' : '')  , 'id' => 'name', 'required' => 'required', 'placeholder'=>\App\Help::translate('name',$local)]) }}
                <small class="text-danger">{{ $errors->first('name') }}</small>
              </div>
            </div>
            @if(auth()->user()->is_social !=1)
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('email',$local)}}</label>
                {{ Form::email('email', isset($row) ? $row->email : '', ['class'=>' form-control' . ($errors->has('email') ? 'redborder' : '')  , 'id' => 'email', 'required' => 'required', 'placeholder'=>\App\Help::translate('email',$local)]) }}
                <small class="text-danger">{{ $errors->first('email') }}</small>
              </div>
            </div>
            @endif
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('mobile',$local)}}</label>
                {{ Form::text('mobile', isset($row) ? $row->mobile : '', ['class'=>' form-control' . ($errors->has('mobile') ? 'redborder' : '')  , 'onkeypress'=>'return onlyNumberKey(event)','id' => 'mobile', 'placeholder'=>\App\Help::translate('mobile',$local)]) }}
                <small class="text-danger">{{ $errors->first('mobile') }}</small>
              </div>
            </div>
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('address',$local)}}</label>
                {{ Form::text('address', isset($row) ? $row->address : '', ['class'=>' form-control' . ($errors->has('address') ? 'redborder' : '')  , 'id' => 'address', 'placeholder'=>\App\Help::translate('address',$local)]) }}
                <small class="text-danger">{{ $errors->first('address') }}</small>
              </div>
            </div> -->
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('country',$local)}}</label>
                {{ Form::select('country_id',$countries, isset($row) ? $row->country_id : [], ['class'=>' form-control' . ($errors->has('countries') ? 'redborder' : '')  , 'id' => 'countries', 'required' => 'required','placeholder'=> \App\Help::translate('select_country',$local)]) }}
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputName1">{{\App\Help::translate('city',$local)}}</label>
                {{ Form::select('city_id',$cities, isset($row) ? $row->city_id : [], ['class'=>' form-control' . ($errors->has('cities') ? 'redborder' : '')  , 'id' => 'cities', 'required' => 'required','placeholder'=> \App\Help::translate('select_city',$local)]) }}
                <small class="text-danger">{{ $errors->first('city_id') }}</small>
              </div>
            </div> -->
          </div>
        </div>
        <div class="col-3">
          <div class="row">
            <div class="col-md-12 d-flex justify-content-center">
              <div class="form-group">
                @if(isset($row->image))
                  <img src="{{$row->image}}" id="picture" style="width: 205px;height: 205px;border-radius: 50%;" alt="{{$row->image}}">
                @else
                  <img src="/assets/images/users/default.png" id="picture" style="width: 205px;height: 205px;border-radius: 50%;" alt="">
                @endif
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                {{ Form::file('image', ['class'=>'file-upload-default' . ($errors->has('image') ? 'redborder' : '')  , 'id' => 'image', 'placeholder'=>'image']) }}
                <div class="input-group col-xs-12 w-100">
                  <span class="input-group-append w-100">
                    <button class="file-upload-browse btn btn-gradient-primary w-100" type="button">{{\App\Help::translate('upload',$local)}}</button>
                    <input type="text" class="form-control file-upload-info border-0 " disabled placeholder="">
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-6">
          <button type="submit" class="btn btn-gradient-primary me-2">{{ \App\Help::translate('submit',$local) }}</button>
          <a href="{{route('panel.index')}}" class="btn btn-light">{{ \App\Help::translate('cancel',$local) }}</a>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection
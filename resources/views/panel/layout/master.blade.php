<!DOCTYPE html>
<html>
  <head>
    @include('panel.includes.header')
  </head>
  <body>
    <div class="container-scroller" id="eventiqr-panel">
      <!-- partial:partials/_navbar.html -->
        @include('panel.includes.navbar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        @include('panel.includes.sidebar')
        
        <div class="main-panel">
          <div class="content-wrapper">
            @yield('content')
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
            @include('panel.includes.footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    @include('panel.includes.scripts')

  </body>
</html>



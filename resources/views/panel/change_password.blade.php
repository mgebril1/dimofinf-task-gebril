@extends('panel.layout.master')
@section('content')
<div class="col-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
          <h4 class="card-title">{{ \App\Help::translate('change_password',$local)}}</h4>
      {!! Form::open(['route' => 'panel.change_password', 'class'=>'forms-sample' ,'files' => true]) !!}
      <br>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="exampleInputName1">{{\App\Help::translate('old_password',$local)}}</label>
              <input type="password" name="old_password" class="form-control {{($errors->has('old_password') ? 'redborder' : '')}}" required>
              <small class="text-danger">{{ $errors->first('old_password') }}</small>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="exampleInputName1">{{\App\Help::translate('password',$local)}}</label>
              <input type="password" name="password" class="form-control {{($errors->has('password') ? 'redborder' : '')}}" required>
              <small class="text-danger">{{ $errors->first('password') }}</small>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label >{{\App\Help::translate('password_confirmation',$local)}}</label>
                <input type="password" name="password_confirmation" class="form-control {{($errors->has('password_confirmation') ? 'redborder' : '')}}" required>
              <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
            </div>
          </div>
        </div>

        <button type="submit" class="btn btn-gradient-primary me-2">{{ \App\Help::translate('submit',$local) }}</button>
        <a href="{{route('panel.index')}}" class="btn btn-light">{{ \App\Help::translate('cancel',$local) }}</a>
        {!! Form::close() !!}
      </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection